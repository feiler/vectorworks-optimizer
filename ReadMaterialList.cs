﻿using System;
using System.Text;
using System.IO;
using System.Windows;
using CsvHelper;
using System.Globalization;
using CsvHelper.Configuration.Attributes;

namespace Vectorworks_Optimizer
{
    internal class ReadMaterialList
    {
        [Index(0)]
        public string ProjektNr { get; set; }
        [Index(1)]
        public string PositionNr { get; set; }
        [Index(3)]
        public string FurnitureName { get; set; }
        [Index(4)]
        public string CarrierMaterial { get; set; }
        [Index(5)]
        public string Designation { get; set; }
        [Index(6)]
        public float Lenght { get; set; }
        [Index(7)]
        public float Width { get; set; }
        [Index(8)]
        public float Thick { get; set; }
        [Index(9)]
        public int Count { get; set; }
        [Index(10)]
        public string ComponentName { get; set; }
        [Index(11)]
        public string edgeFront { get; set; }
        [Index(13)]
        public string edgeBack { get; set; }
        [Index(15)]
        public string edgeLeft { get; set; }
        [Index(17)]
        public string edgeRight { get; set; }
        [Index(19)]
        public string PolygonalEdge { get; set; }
        [Index(20)]
        public string ExistEdge { get; set; }
        [Index(21)]
        public string SurfaceOne { get; set; }
        [Index(22)]
        public string SurfaceTwo { get; set; }
        [Index(23)]
        public string Coating { get; set; }
        [Index(27)]
        public string CNCFormatting { get; set; }
        [Index(28)]
        public string CNCMain { get; set; }
        [Index(29)]
        public string CNCNext { get; set; }

        public CsvReader ReadCsvFile(string filename)
        {
          
            StreamReader sr = new StreamReader(filename, Encoding.UTF7);    
            return new CsvReader(sr, CultureInfo.InvariantCulture);
            

        }
        public void voidReadCsvFile(string filename)
        {
            MaterialList materialList = new MaterialList();
            StreamReader sr = new StreamReader(filename, Encoding.UTF7);
            
            CsvReader csv = new CsvReader(sr, CultureInfo.InvariantCulture);
            
            var csvRecord = csv.GetRecords<ReadMaterialList>();

            foreach (var currentLine in csvRecord)
            {
                if (currentLine.Designation == "Belag")
                {
                    //fill belag
                }
                else if(currentLine.Width==0  
                    && currentLine.Thick==0 
                    && currentLine.CNCMain == "")
                {
                    //Beschlag
                }
                else
                {
                   
                }
            }
        }

    }
}
