﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;



namespace Vectorworks_Optimizer
{
    /// <summary>
    /// Interaction logic for OptionGeneral.xaml
    /// </summary>
    
    public partial class OptionGeneral : Window
    {
        
        public OptionGeneral()
        {
            InitializeComponent();
            
            getSave();
        }
        void getSave()
        {
            
            tb_rootFolderCNC.Text = OptionGeneralSetting.Default.rootFolderCNC;
            tb_ncCodeEnd.Text = OptionGeneralSetting.Default.ncCodeEnd;
            tb_miniSizeLength.Text = OptionGeneralSetting.Default.miniSizeLength;
            tb_miniSizeWidth.Text = OptionGeneralSetting.Default.miniSizeWidth;
            cb_doShelfboardSummarize.IsChecked = OptionGeneralSetting.Default.doShelfboardSummarize;
            tb_shelfboardName.Text = OptionGeneralSetting.Default.shelfboardName;
        }

        private void bt_save_Click(object sender, RoutedEventArgs e)
        {
            OptionGeneralSetting.Default.rootFolderCNC = tb_rootFolderCNC.Text;
            OptionGeneralSetting.Default.ncCodeEnd =tb_ncCodeEnd.Text ;
            OptionGeneralSetting.Default.miniSizeLength = tb_miniSizeLength.Text;
            OptionGeneralSetting.Default.miniSizeWidth = tb_miniSizeWidth.Text;
            OptionGeneralSetting.Default.doShelfboardSummarize = cb_doShelfboardSummarize.IsChecked == true;
            OptionGeneralSetting.Default.shelfboardName = tb_shelfboardName.Text;
            OptionGeneralSetting.Default.Save();
            Close();
        }

        private void button_close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
