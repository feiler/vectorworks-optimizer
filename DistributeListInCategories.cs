﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vectorworks_Optimizer
{
    class DistributeListInCategories
    {
        public List<string> collectMaterial(List<string> wholeList)
        {
            List<string> temp = new List<string>();
            foreach( string currentLine in wholeList)
            {
                if (!currentLine.Contains(" Belag 1,") &&
                    !currentLine.Contains(" Belag 2,") &&
                    !currentLine.Contains(",0,0,"))
                { temp.Add(currentLine); }
            }

            return temp;
        }
        public List<string> collectSurface(List<string> wholeList)
        {
            List<string> temp = new List<string>();
            foreach (string currentLine in wholeList)
            {
                if (currentLine.Contains(" Belag 1,") ||
                    currentLine.Contains(" Belag 2,") )
                    { temp.Add(currentLine); }
            }

            return temp;
        }
        public List<string> collectFittings(List<string> wholeList)
        {
            List<string> temp = new List<string>();
            foreach (string currentLine in wholeList)
            {
                if (currentLine.Contains(",0,0,"))
                { temp.Add(currentLine); }
            }

            return temp;
        }

    }
}
