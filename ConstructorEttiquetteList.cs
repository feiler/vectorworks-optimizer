﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vectorworks_Optimizer
{
    class ConstructorEttiquetteList
    {
        public int ComponentNr { get; set; }
        public string ProjectID { get; set; }
        public string PojectPos { get; set; }
        public string FurnitureName { get; set; }
        public string ComponentDefinition { get; set; }
        public string ComponentName { get; set; }
        public string CarrierMaterial { get; set; }
        public int Count { get; set; }
        public float FinalLength { get; set; }
        public float FinalWidth { get; set; }
        public float Thick { get; set; }
        public float CuttingLength { get; set; }
        public float CuttingWidth { get; set; }
        public bool Rotatable { get; set; }
        public string EdgeFront { get; set; }
        public string EdgeBack { get; set; }
        public string EdgeLeft { get; set; }
        public string EdgeRight { get; set; }
        public string EdgeCode { get; set; }
        public string EdgeVN { get; set; }
        public string EdgePoly { get; set; }
        public string EdgeExist { get; set; }
        public string SurfaceH { get; set; }
        public string SurfaceN { get; set; }
        public string Coating { get; set; }
        public string NCMain { get; set; }
        public string NCNext { get; set; }
        public string NCMain2 { get; set; }
        public string NCNext2 { get; set; }
        public string NCForming { get; set; }
        public string CustomerLastName { get; set; }
        public string ProjectName { get; set; }
        public bool EdgeBevorSurface { get; set; }
        public bool HingedSideLength { get; set; }
    }
}
