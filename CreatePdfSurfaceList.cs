﻿using iText.Barcodes;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Action;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Kernel.Font;
using System;
using System.Collections.Generic;
using System.Text;
using iText.IO.Font;
namespace Vectorworks_Optimizer
{
    class CreatePdfSurfaceList
    {
        private float edgeSpacing = 20f;
        private string pdfNameTemp = "";
        private string groundPath = OptionGeneralSetting.Default.rootFolderCNC;
        PdfDocument pdfDoc;
        Document doc;
        PdfFont fontBold = PdfFontFactory.CreateFont(FontConstants.HELVETICA_BOLD);
        public void pOpen(string path, string pdfName)
        {
            pdfNameTemp = pdfName;
            PdfWriter writer = new PdfWriter(path + @"\" + pdfName + ".pdf");
            pdfDoc = new PdfDocument(writer);
            pdfDoc.SetDefaultPageSize(PageSize.A4);

            doc = new Document(pdfDoc).SetFontSize(8);
            doc.SetMargins(edgeSpacing, edgeSpacing, edgeSpacing, edgeSpacing);


        }
        public void addHeaderTableMaterial(string dictionaryPath, string projektNr, string customerName)
        {
            Table table = new Table(new float[] { 1f, 2f, 4f }, true);
            Cell cell = new Cell().Add(new Paragraph(pdfNameTemp));
            table.AddCell(cell);
            cell = new Cell().SetHorizontalAlignment(HorizontalAlignment.RIGHT).Add(new Paragraph("Projekt-Nr: " + projektNr + "  " + customerName));
            table.AddCell(cell);
            cell = new Cell().Add(new Paragraph("OrdnerPfad" + dictionaryPath));
            table.AddCell(cell);
            doc.Add(table);

        }
        public void addHeaderTableMaterialInfo()
        {
            float[] pointColumnWidths = { 0.5f, 3f, 0.5f,0.3f, 0.5f, 2f, 0.5f, 1f, 1f };
            Table table = new Table(UnitValue.CreatePercentArray(pointColumnWidths)).UseAllAvailableWidth();

            Table tablePosPartInfo = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            tablePosPartInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderBottom(new SolidBorder(1)).Add(new Paragraph("Pos")));
            tablePosPartInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("BauT")));
            table.AddCell(tablePosPartInfo);
            table.AddCell("Zugehörigkeit");
            table.AddCell("Seite");
            table.AddCell("Stk");
            table.AddCell("Material");
            Table tableCutInfo = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            tableCutInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("Zuschnittmass")));
            Table tableSizeInfo = new Table(UnitValue.CreatePercentArray(2)).UseAllAvailableWidth().SetPaddingLeft(-1).SetPaddingRight(-1);
            tableSizeInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderRight(new SolidBorder(1)).Add(new Paragraph("Länge")));
            tableSizeInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("Breite")));
            tableCutInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(tableSizeInfo));
            table.AddCell(tableCutInfo);
            Table tableCutDone = new Table(1);
            tableCutDone.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderBottom(new SolidBorder(1)).Add(new Paragraph("ZS")));
            tableCutDone.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("CNC")));
            table.AddCell(tableCutDone);
            table.AddCell("CNC-1");
            table.AddCell("CNC-2");

            doc.Add(table);
        }
        public void AddTableMaterial(
            string ComponentNr,
            string projectNr,
            string positionNr,
            string side,
            string carrierMaterial,
            string designation,
            int count,
            float cuttingLength, float cuttingWidth,
            string componentName,
            string CNC1, string CNC2,
            string getBasicMaterialNumber)
        {
            float[] pointColumnWidths = { 0.5f, 3f, 0.5f, 0.3f, 0.5f, 2f, 0.5f, 1f, 1f };
            Table table = new Table(UnitValue.CreatePercentArray(pointColumnWidths)).UseAllAvailableWidth();

            Table tablePosPartInfo = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            tablePosPartInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderBottom(new SolidBorder(1)).Add(new Paragraph(positionNr)));
            tablePosPartInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph(ComponentNr + "")));
            table.AddCell(tablePosPartInfo);
            Table tableComponentInfo = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            tableComponentInfo.AddCell(componentName);
            tableComponentInfo.AddCell("BauTNr: "+getBasicMaterialNumber);
            table.AddCell(tableComponentInfo);
            table.AddCell(side);
            table.AddCell(count+"");
            table.AddCell(carrierMaterial);
            Table tableSizeInfo = new Table(UnitValue.CreatePercentArray(2)).UseAllAvailableWidth().SetPaddingLeft(-1).SetPaddingRight(-1);
            tableSizeInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderRight(new SolidBorder(1)).Add(new Paragraph(cuttingLength+"")));
            tableSizeInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph(cuttingWidth+"")));
            table.AddCell(tableSizeInfo);
            Table tableCutDone = new Table(1);
            tableCutDone.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderBottom(new SolidBorder(1)).Add(new Paragraph("0")));
            tableCutDone.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("0")));
            table.AddCell(tableCutDone);
            if (CNC1 != "")
            {
                table.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(qrCodeGenerate(CNC1 + OptionGeneralSetting.Default.ncCodeEnd).SetBorder(new DottedBorder(0.5f))));
            }
            else
            {
                table.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorder(new DottedBorder(0.5f)));
            }
            if (CNC2 != "")
            {
                table.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(qrCodeGenerate(CNC2 + OptionGeneralSetting.Default.ncCodeEnd).SetHorizontalAlignment(HorizontalAlignment.RIGHT).SetBorder(new DottedBorder(0.5f))));
            }
            else
            {
                table.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorder(new DottedBorder(0.5f)));
            }

            doc.Add(table);

            
        }
            public void pClose()
        {
            doc.Close();
        }
        private Image qrCodeGenerate(string path, string projectNR)
        {
            BarcodeQRCode barcodeQRCode = new BarcodeQRCode();
            barcodeQRCode.SetCode(groundPath + @"\" + projectNR + @"\" + path);
            Image qrCodeImeage = new Image(barcodeQRCode.CreateFormXObject(pdfDoc));
            qrCodeImeage.ScaleAbsolute(30, 30);
            return qrCodeImeage;
        }
        private Image qrCodeGenerate(string NCCode)
        {
            BarcodeQRCode barcodeQRCode = new BarcodeQRCode();
            barcodeQRCode.SetCode(NCCode);
            Image qrCodeImeage = new Image(barcodeQRCode.CreateFormXObject(pdfDoc));
            qrCodeImeage.ScaleAbsolute(30, 30);
            return qrCodeImeage;
        }
    }
}
