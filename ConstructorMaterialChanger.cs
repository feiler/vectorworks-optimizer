﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vectorworks_Optimizer
{
    public class ConstructorMaterialChanger
    {
        
        public string Category { get; set; }
        public string CarrierMaterial { get; set; }
        public string Thick { get; set; }
        public string NewCarrierMaterial { get; set; }
        public string TypeMaterial { get; set; }
        public string EditType { get; set; }
        public string Format { get; set; }
        public bool EdgeBevorSurface { get; set; }

        public string[] categoryType = {"Platte",
                                        "Kante",
                                        "Belag",
                                        "Oberfläche"};
        public string[,] typeMaterialArray = { {"","",""},
                                                {"custome","Standart", "Dia"},
                                                { "solid","Massiv", "Ma" },
                                                { "veneer","Furnierte Platte", "Ma" },
                                                { "melamine","belegte Platte", "Dia" },
                                                { "gw","Leimholz", "Ma" },
                                                { "3l","3Schicht", "Ma" },
                                                { "glass","Glass","keine"  },
                                                { "pole","Stange","keine" },
                                                { "help","Hilfsmaterial","keine"  } };
        public string[,] typeEdgeType = { { "", "keine" },
                                            { "ABS", "Dia" },
                                            {"Einleimer","Ma" } };
        public string[,] typeSurfaceType = { { "", "keine" },
                                            { "Kunstharz","Dia" },
                                            { "Furnier","Ma" } };
        public string[] editTypeArray = { "","keine","Dia", "Ma" };
        public string[] formatArray = {"",
                                        "2800x2070",
                                        "4100x2070",
                                        "5000x1050",
                                        "5000x2050",
                                        "3050x1300",
                                        "2800x1300",
                                        "2600x1300"};
        
    }
}
