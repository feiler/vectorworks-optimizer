﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace Vectorworks_Optimizer
{
    class CreatePdfList
    {
        private List<ConstructorMaterialList> constructorMaterialLists = new List<ConstructorMaterialList>();
        private List<ConstructorSurfaceList> constructorSurfaceLists = new List<ConstructorSurfaceList>();
        private ConstructorMaterialChanger constructorMaterialChangerVariabel = new ConstructorMaterialChanger();
        private List<string> controllList = new List<string>();
        private string surfaceGroundName = "Belag";
        
        public void loadList(List<ConstructorMaterialList> constructorMaterialListsLoad, List<ConstructorSurfaceList> constructorSurfaceListsLoad)
        {
            constructorMaterialLists = constructorMaterialListsLoad;
            constructorSurfaceLists = constructorSurfaceListsLoad;
        }
        public void sortListAndChangeMaterial(List<ConstructorMaterialChanger> constructorMaterialChangers)
        {
            foreach (var currentLine in constructorMaterialChangers)
            {
                if (currentLine.NewCarrierMaterial != "")
                {
                    foreach (var currentLine2 in constructorMaterialLists)
                    {
                        if (currentLine2.CarrierMaterial == currentLine.CarrierMaterial && currentLine.Category == constructorMaterialChangerVariabel.categoryType[0])
                        {
                            currentLine2.CarrierMaterial = currentLine2.CarrierMaterial = currentLine.NewCarrierMaterial + " " + currentLine2.Thick;
                            currentLine2.EditType = currentLine2.EditType = currentLine.EditType;
                        }
                        if (currentLine.Category == constructorMaterialChangerVariabel.categoryType[1])
                        {

                            currentLine2.EdgeBack= currentLine2.EdgeBack.Replace(currentLine.CarrierMaterial, currentLine.NewCarrierMaterial);
                            currentLine2.EdgeFront = currentLine2.EdgeFront.Replace(currentLine.CarrierMaterial, currentLine.NewCarrierMaterial);
                            currentLine2.EdgeLeft= currentLine2.EdgeLeft.Replace(currentLine.CarrierMaterial, currentLine.NewCarrierMaterial);
                            currentLine2.EdgeRight= currentLine2.EdgeRight.Replace(currentLine.CarrierMaterial, currentLine.NewCarrierMaterial);
                            currentLine2.EdgePoly= currentLine2.EdgePoly.Replace(currentLine.CarrierMaterial, currentLine.NewCarrierMaterial);
                        }
                        if (currentLine.Category == constructorMaterialChangerVariabel.categoryType[2])
                        {
                            currentLine2.SurfaceH= currentLine2.SurfaceH.Replace(currentLine.CarrierMaterial, currentLine.NewCarrierMaterial);
                            currentLine2.SurfaceN= currentLine2.SurfaceN.Replace(currentLine.CarrierMaterial, currentLine.NewCarrierMaterial);
                        }
                        if (currentLine.Category == constructorMaterialChangerVariabel.categoryType[3])
                        {
                            currentLine2.Coating= currentLine2.Coating.Replace(currentLine.CarrierMaterial, currentLine.NewCarrierMaterial);
                            
                        }
                    }
                    foreach (var currentLine2 in constructorSurfaceLists)
                    {
                        if (currentLine.Category == constructorMaterialChangerVariabel.categoryType[2])
                        {
                            currentLine2.CarrierMaterial = currentLine2.CarrierMaterial.Replace(currentLine.CarrierMaterial, currentLine.NewCarrierMaterial);
                           
                        }
                    }
                }
            }
            sortListen();
        }
        private void sortListen() {

            constructorMaterialLists = constructorMaterialLists.OrderByDescending(v => v.CuttingWidth).ToList();

            constructorMaterialLists = constructorMaterialLists.OrderByDescending(v => v.CuttingLength).ToList();

            constructorMaterialLists = (from material in constructorMaterialLists
                      orderby material.CarrierMaterial
                      ascending
                      select material).ToList();


            constructorSurfaceLists = constructorSurfaceLists.OrderByDescending(v => v.CuttingWidth).ToList();

            constructorSurfaceLists = constructorSurfaceLists.OrderByDescending(v => v.CuttingLength).ToList();

            constructorSurfaceLists = (from material in constructorSurfaceLists
                                       orderby material.CarrierMaterial
                                        ascending
                                        select material).ToList();



        }
        public void distributeMaterial(string path, string pdfName, string oldPath,string newPath)
        {
            CreatePdfMaterialList createPdfMaterialList = new CreatePdfMaterialList();
            CncCopieForScanner cncCopieForScanner = new CncCopieForScanner();
            bool openMaterialList = true;
            int componentNr = 1;
            foreach(var currentLine in constructorMaterialLists)
            {
                
                currentLine.ComponentNumber= componentNr++;
                if (openMaterialList)
                {
                    createPdfMaterialList.pOpen(path, pdfName);
                    createPdfMaterialList.addHeaderTableMaterial(path, currentLine.ProjectID,currentLine.CustomerLastName);
                    createPdfMaterialList.addHeaderTableMaterialInfo();
                    openMaterialList = false;
                }
                if (OptionOptimizerCNCToolsSetting.Default.doToolAdjustActivation == false)
                {
                    currentLine.NCMain = cncCopieForScanner.editNameCopyGetNewPath(oldPath, currentLine.PojectPos, currentLine.FurnitureName, currentLine.NCMain, newPath, componentNr, "H");
                    currentLine.NCNext = cncCopieForScanner.editNameCopyGetNewPath(oldPath, currentLine.PojectPos, currentLine.FurnitureName, currentLine.NCNext, newPath, componentNr, "N");
                    currentLine.NCMain2 = cncCopieForScanner.editNameCopyGetNewPath(oldPath, currentLine.PojectPos, currentLine.FurnitureName, currentLine.NCMain2, newPath, componentNr, "H");
                    currentLine.NCNext2 = cncCopieForScanner.editNameCopyGetNewPath(oldPath, currentLine.PojectPos, currentLine.FurnitureName, currentLine.NCNext2, newPath, componentNr, "N");
                }
                else
                {
                    CNCOptimationWoodFlash cNCOptimationWoodFlash = new CNCOptimationWoodFlash();
                    cNCOptimationWoodFlash.cncOptimation(currentLine.NCForming, currentLine.NCMain, currentLine.NCNext,
                                                            oldPath, Regex.Match(currentLine.PojectPos, @"\d+")+"", currentLine.FurnitureName, newPath, currentLine.ComponentNumber.ToString("000"), currentLine.ComponentDefinition,
                                                            currentLine.Thick, currentLine.EdgeFront, currentLine.EdgeBack,
                                                            currentLine.EdgeLeft, currentLine.EdgeRight,
                                                            currentLine.EdgePoly, currentLine.EdgeExist, currentLine.EditType, currentLine.EdgeBevorSurface, controllList);
                    currentLine.NCMain = cNCOptimationWoodFlash.getNCMainPath();
                    currentLine.NCMain2 = cNCOptimationWoodFlash.getNCMain2Path();
                    currentLine.NCNext = cNCOptimationWoodFlash.getNCNextPath();
                    currentLine.NCNext2 = cNCOptimationWoodFlash.getNCNext2Path();
                    
                    controllList = cNCOptimationWoodFlash.getControllList();
                    
                    if (currentLine.FinalLength == cNCOptimationWoodFlash.getHingedSide()) currentLine.HingedSideLength = true;
                    else currentLine.HingedSideLength = false;
                    
                }

                createPdfMaterialList.AddTableMaterial(
                    currentLine.ComponentNumber+"",
                    currentLine.ProjectID,
                    currentLine.PojectPos,
                    currentLine.FurnitureName,
                    currentLine.CarrierMaterial,
                    currentLine.ComponentDefinition,
                    currentLine.Count,
                    currentLine.FinalLength, currentLine.FinalWidth, currentLine.Thick,
                    currentLine.CuttingLength, currentLine.CuttingWidth,
                    currentLine.ComponentName,
                    currentLine.EdgeFront, currentLine.EdgeBack,
                    currentLine.EdgeLeft, currentLine.EdgeRight,
                    currentLine.EdgePoly, currentLine.EdgeExist,
                    currentLine.SurfaceH, currentLine.SurfaceN,
                    currentLine.Coating,
                    currentLine.NCMain, currentLine.NCMain2, currentLine.NCNext, currentLine.NCNext2) ;
            }
            createPdfMaterialList.pClose();
        }
        public void distributeEttiquette(string path, string pdfName,int startfield)
        {
            
            CreatePdfEttiquetteList createPdfEttiquetteList = new CreatePdfEttiquetteList();
            createPdfEttiquetteList.pOpen(path, pdfName);
            for(int i=1; i < startfield; i++)
            {
                createPdfEttiquetteList.freePlaceEttiquette();
            }

            foreach (var currentLine in constructorMaterialLists)
            {
                string[] tempEdgeCode = currentLine.EdgeCode.Replace("0", "").Split(";");
                createPdfEttiquetteList.createEttiquette(
                    "Bauteilname",
                    "BauTNr",
                    currentLine.ComponentNumber+"",
                    currentLine.ProjectID,
                    currentLine.PojectPos,
                    currentLine.FurnitureName,
                    currentLine.CarrierMaterial,
                    currentLine.ComponentDefinition,
                    currentLine.Count,
                    currentLine.FinalLength, currentLine.FinalWidth, currentLine.Thick,
                    currentLine.CuttingLength, currentLine.CuttingWidth,
                    currentLine.Rotatable.Equals("yes") ? true : false ,
                    currentLine.ComponentName,
                    currentLine.EdgeFront +"-"+ tempEdgeCode[0], currentLine.EdgeBack + "-" + tempEdgeCode[1] + "      ",
                    currentLine.EdgeLeft + "-" + tempEdgeCode[2], currentLine.EdgeRight + "-" + tempEdgeCode[3],
                    currentLine.EdgePoly, currentLine.EdgeExist,
                    currentLine.SurfaceH, currentLine.SurfaceN,
                    currentLine.Coating,
                    currentLine.NCMain, currentLine.NCNext, currentLine.NCMain2, currentLine.NCNext2,
                    currentLine.CustomerLastName,
                    "",
                    currentLine.HingedSideLength);
            }
             
            foreach (var currentLine in constructorSurfaceLists)
            {
                string[] tempEdgeCode = currentLine.EdgeCode.Replace("0","").Split(";"); 
                string sideName = "";
                if (currentLine.ComponentName.Contains(" Belag 1")) sideName = "Belag H/Innen";
                else sideName = "Belag N/Aussen";
                createPdfEttiquetteList.createEttiquette(
                                    sideName,
                                    "BelTNr",
                                    currentLine.ComponentNumber+"",
                                    currentLine.ProjectID,
                                    currentLine.PojectPos,
                                    currentLine.FurnitureName
                                    +" / "+ currentLine.ComponentName.Replace(" Belag 1", "").Replace(" Belag 2", ""),
                                    currentLine.CarrierMaterial,
                                    "",
                                    currentLine.Count,
                                    0,0, Convert.ToSingle(currentLine.Thick),
                                    currentLine.CuttingLength, currentLine.CuttingWidth,
                                    currentLine.Rotatable.Equals("yes") ? true : false,
                                    currentLine.ComponentName,
                    currentLine.EdgeFront + "-" + tempEdgeCode[0], currentLine.EdgeBack + "-" + tempEdgeCode[1]+"      ",
                    currentLine.EdgeLeft + "-" + tempEdgeCode[2], currentLine.EdgeRight + "-" + tempEdgeCode[3],
                                    "","","","","",
                                    currentLine.NC1, currentLine.NC2,"","",
                                    currentLine.CustomerLastName,
                                    "BauTNr: "+currentLine.getBasicMaterialNumber,
                                    currentLine.HingedSideLength);
            }

                createPdfEttiquetteList.AddTable();


            createPdfEttiquetteList.pClose();
        }
        public void distributeFitting(List<ConstructorFittingList> constructorFittingLists, string path, string pdfName)
        {
            
                CreatePdfFittingList createPdfFittingList = new CreatePdfFittingList();
                bool openMaterialList = true;
                foreach (var currentLine in constructorFittingLists)
                {
                    if (openMaterialList)
                    {
                        createPdfFittingList.pOpen(path, pdfName);
                        createPdfFittingList.addHeaderTableMaterial(currentLine.ProjectName, currentLine.ProjectID, currentLine.CustomerLastName);
                        createPdfFittingList.addHeaderTableMaterialInfo();
                        openMaterialList = false;
                    }
                    createPdfFittingList.AddTableMaterial(
                        currentLine.PojectPos, currentLine.PosDescription,
                        currentLine.FurnitureName, currentLine.ComponentDefinition, currentLine.ComponentName,
                        currentLine.ItemsNR,
                        currentLine.Count, currentLine.CuttingLength);
                }
                createPdfFittingList.pClose();
           
        }
        public void distributeSurface(string path, string pdfName, string oldPath, string newPath)
        {
            CreatePdfSurfaceList createPdfSurfaceList = new CreatePdfSurfaceList();
            fillNcToSurface();

            bool openMaterialList = true;
            int componentNr = 1;
            foreach (var currentLine in constructorSurfaceLists)
            {
                currentLine.ComponentNumber = componentNr++;
                
                if (openMaterialList)
                {
                    createPdfSurfaceList.pOpen(path, pdfName);
                    createPdfSurfaceList.addHeaderTableMaterial(path, currentLine.ProjectID, currentLine.CustomerLastName);
                    createPdfSurfaceList.addHeaderTableMaterialInfo();
                    openMaterialList = false;
                }
                createPdfSurfaceList.AddTableMaterial(
                        currentLine.ComponentNumber+"",
                        currentLine.ProjectID,
                        currentLine.PojectPos,
                        currentLine.Side,
                        currentLine.CarrierMaterial,
                        currentLine.ComponentDefinition,
                        currentLine.Count,
                        currentLine.CuttingLength, currentLine.CuttingWidth,
                        currentLine.ComponentName,
                        currentLine.NC1, currentLine.NC2,
                        currentLine.getBasicMaterialNumber+"");



            }
            createPdfSurfaceList.pClose();
        }
        public void distributeOptimizList (string path, string namePdfAddition, List<ConstructorMaterialChanger> constructorMaterialChangers)
        {
           
            CreateOptimizationListRolling createOptimizationListRolling = new CreateOptimizationListRolling();
            CreateOptimizationListMaterial createOptimizationListMaterial = new CreateOptimizationListMaterial();
            if (constructorMaterialChangers.Count != 0) createOptimizationListRolling.writeOptimizationRollingFile(path, namePdfAddition, constructorMaterialChangers);
            else MessageBox.Show("Materialwechsler war leer, darum wurde keine Rollingliste geschrieben!");
            createOptimizationListMaterial.writeOptimizationListFile(path, namePdfAddition, constructorMaterialLists, constructorSurfaceLists);
        }
        private void fillNcToSurface()
        {
            foreach(var currentLine in constructorSurfaceLists)
            {
                foreach (var currentLine2 in constructorMaterialLists)
                {
                    if (currentLine2.ComponentName + " " + surfaceGroundName + " 1" == currentLine.ComponentName)
                    {
                        currentLine.NC1 = currentLine2.NCMain;
                        currentLine.NC2 = currentLine2.NCMain2;
                        currentLine.getBasicMaterialNumber = currentLine2.ComponentNumber;
                        currentLine.Side = "H/In";
                        if (currentLine.NC1 != "")
                        {
                            currentLine.EdgeFront = currentLine2.EdgeFront;
                            currentLine.EdgeBack = currentLine2.EdgeBack;
                            currentLine.EdgeLeft= currentLine2.EdgeLeft;
                            currentLine.EdgeRight = currentLine2.EdgeRight;
                        }
                        currentLine.HingedSideLength = currentLine2.HingedSideLength;
                        currentLine.EdgeCode = currentLine2.EdgeCode;
                        break;
                    }
                    if (currentLine2.ComponentName + " " + surfaceGroundName + " 2" == currentLine.ComponentName)
                    {
                        currentLine.NC1 = currentLine2.NCNext;
                        currentLine.NC2 = currentLine2.NCNext2;
                        currentLine.getBasicMaterialNumber = currentLine2.ComponentNumber;
                        currentLine.Side = "N/Aus";
                        if(currentLine.NC1!="" && currentLine2.NCMain == "")
                        {
                            currentLine.EdgeFront = currentLine2.EdgeFront;
                            currentLine.EdgeBack = currentLine2.EdgeBack;
                            currentLine.EdgeLeft = currentLine2.EdgeLeft;
                            currentLine.EdgeRight = currentLine2.EdgeRight;
                        }
                        currentLine.EdgeCode = currentLine2.EdgeCode;
                        currentLine.HingedSideLength = currentLine2.HingedSideLength;
                        break;
                    }
                }
            }
        }
    }
}
