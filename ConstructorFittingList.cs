﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vectorworks_Optimizer
{
    class ConstructorFittingList
    {
        public string ProjectID { get; set; }
        public string PojectPos { get; set; }
        public string PosDescription { get; set; }
        public string FurnitureName { get; set; }
        public string ComponentDefinition { get; set; }
        public string ComponentName { get; set; }
        public string ItemsNR { get; set; }
        public int Count { get; set; }
        public float CuttingLength { get; set; }
        public string CustomerLastName { get; set; }
        public string ProjectName { get; set; }
        public string FileName { get; set; }
    }
}
