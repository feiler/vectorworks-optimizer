﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Vectorworks_Optimizer
{
    /// <summary>
    /// Interaction logic for EttiquetteJumpOverSpace.xaml
    /// </summary>
    public partial class EttiquetteJumpOverSpace : Window
    {
        public EttiquetteJumpOverSpace()
        {
            InitializeComponent();
        }

        
        private void tb_startFeld_PreviewTextInput(System.Object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = IsTextNumeric(e.Text);

        }


        private static bool IsTextNumeric(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9]");
            return reg.IsMatch(str);

        }
        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            if(Convert.ToInt32(tb_startFeld.Text) <= 21) 
                 this.DialogResult = true;
            else  MessageBox.Show("Zahl zu gross"); 
            
        }

        public int getPdfNumber()
        {
            if (tb_startFeld.Text == "" || tb_startFeld.Text == "0")
            {
                return 1;
            }
            else
            {
                return + Convert.ToInt32(tb_startFeld.Text);
            }

        }
    }
}
