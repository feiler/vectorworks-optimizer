﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Vectorworks_Optimizer
{
    /// <summary>
    /// Interaction logic for OptionOptimizerCNC.xaml
    /// </summary>
    public partial class OptionOptimizerCNC : Window
    {
        public OptionOptimizerCNC()
        {
            InitializeComponent();
            getSave();
        }
        private void getSave()
        {
            cb_doToolAdjustActivation.IsChecked = OptionOptimizerCNCToolsSetting.Default.doToolAdjustActivation;
            tb_groundToolNr.Text = OptionOptimizerCNCToolsSetting.Default.groundToolNr;
            cb_doGroundToolMiddle.IsChecked = OptionOptimizerCNCToolsSetting.Default.doGroundToolMiddle;
            matDefDia.Text = OptionOptimizerCNCToolsSetting.Default.matDefDia;
            matDefHS.Text = OptionOptimizerCNCToolsSetting.Default.matDefHS;
            squareToolDiaNr.Text = OptionOptimizerCNCToolsSetting.Default.squareToolDiaNr;
            squareToolDiaDistance.Text = OptionOptimizerCNCToolsSetting.Default.squareToolDiaDistance;
            squareToolDiaMaxThick.Text = OptionOptimizerCNCToolsSetting.Default.squareToolDiaMaxThick;
            squareToolDiaMaxDipping.Text = OptionOptimizerCNCToolsSetting.Default.squareToolDiaMaxDipping;
            squareToolDiaMiddle.IsChecked = OptionOptimizerCNCToolsSetting.Default.squareToolDiaMiddle;
            squareToolDiaMinDepth.Text = OptionOptimizerCNCToolsSetting.Default.squareToolDiaMinDepth;
            shapeToolDiaNr.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolDiaNr;
            shapeToolDiaDistance.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolDiaDistance;
            shapeToolDiaMaxThick.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMaxThick;
            shapeToolDiaMaxDipping.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMaxDipping;
            shapeToolDiaMiddle.IsChecked = OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMiddle;
            shapeToolDiaMinDepth.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMinDepth;
            squareToolHSNr.Text = OptionOptimizerCNCToolsSetting.Default.squareToolHSNr;
            squareToolHSDistance.Text = OptionOptimizerCNCToolsSetting.Default.squareToolHSDistance;
            squareToolHMMaxThick.Text = OptionOptimizerCNCToolsSetting.Default.squareToolHSMaxThick;
            squareToolHMMaxDipping.Text = OptionOptimizerCNCToolsSetting.Default.squareToolHSMaxDipping;
            squareToolHSMiddle.IsChecked = OptionOptimizerCNCToolsSetting.Default.squareToolHSMiddle;
            squareToolHSMinDepth.Text = OptionOptimizerCNCToolsSetting.Default.squareToolHSMinDepth;
            shapeToolHSNr.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolHSNr;
            shapeToolHSDistance.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolHSDistance;
            shapeToolHMMaxThick.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolHSMaxThick;
            shapeToolHMMaxDipping.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolHSMaxDipping;
            shapeToolHSMiddle.IsChecked = OptionOptimizerCNCToolsSetting.Default.shapeToolHSMiddle;
            squareToolDiaMinDepth.Text = OptionOptimizerCNCToolsSetting.Default.squareToolDiaMinDepth;
            shapeToolHSMinDepth.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolHSMinDepth;
            squareToolEmergencyNr.Text = OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyNr;
            squareToolEmergencyDistance.Text = OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyDistance;
            squareToolEmergencyMaxThick.Text = OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyMaxThick;
            squareToolEmergencyMaxDipping.Text = OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyMaxDipping;
            squareToolEmergencyMiddle.IsChecked = OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyMiddle;
            squareToolEmergencyMinDepth.Text = OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyMinDepth;
            shapeToolEmergencyNr.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyNr;
            shapeToolEmergencyDistance.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyDistance;
            shapeToolEmergencyMaxThick.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyMaxThick;
            shapeToolEmergencyMaxDipping.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyMaxDipping;
            shapeToolEmergencyMiddle.IsChecked = OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyMiddle;
            shapeToolEmergencyMinDepth.Text = OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyMinDepth;
            tb_mindLength.Text = OptionOptimizerCNCToolsSetting.Default.tb_mindLength +"";
            tb_mindWidth.Text = OptionOptimizerCNCToolsSetting.Default.tb_mindWidth + "";
            cb_doComponentBigger.IsChecked = OptionOptimizerCNCToolsSetting.Default.cb_doComponentBigger;
            cb_doMinEdgeDistance.IsChecked = OptionOptimizerCNCToolsSetting.Default.cb_doMinEdgeDistance;
            tb_minEdgeDistance.Text = OptionOptimizerCNCToolsSetting.Default.tb_minEdgeDistance+"";
            tb_minEdgeDrilling.Text = OptionOptimizerCNCToolsSetting.Default.tb_minEdgeDrilling + "";
            cb_doContourOptimiazation.IsChecked = OptionOptimizerCNCToolsSetting.Default.cb_doContourOptimiazation;



        }

        private void bt_saveAndClose_Click(object sender, RoutedEventArgs e)
        {
            OptionOptimizerCNCToolsSetting.Default.doToolAdjustActivation = cb_doToolAdjustActivation.IsChecked == true;
            OptionOptimizerCNCToolsSetting.Default.groundToolNr = tb_groundToolNr.Text;
            OptionOptimizerCNCToolsSetting.Default.doGroundToolMiddle = cb_doGroundToolMiddle.IsChecked ==true;
            //OptionOptimizerCNCToolsSetting.Default.matDefDia = matDefDia.Text;
            //OptionOptimizerCNCToolsSetting.Default.matDefHS = matDefDia.Text;            
            OptionOptimizerCNCToolsSetting.Default.squareToolDiaNr= squareToolDiaNr.Text ;
            OptionOptimizerCNCToolsSetting.Default.squareToolDiaDistance = squareToolDiaDistance.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolDiaMaxThick = squareToolDiaMaxThick.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolDiaMaxDipping = squareToolDiaMaxDipping.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolDiaMiddle = squareToolDiaMiddle.IsChecked == true;
            OptionOptimizerCNCToolsSetting.Default.shapeToolDiaNr = shapeToolDiaNr.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolDiaMinDepth = squareToolDiaMinDepth.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolDiaDistance = shapeToolDiaDistance.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMaxThick = shapeToolDiaMaxThick.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMaxDipping = shapeToolDiaMaxDipping.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMiddle = shapeToolDiaMiddle.IsChecked == true;
            OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMinDepth = shapeToolDiaMinDepth.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolHSNr = squareToolHSNr.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolHSDistance = squareToolHSDistance.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolHSMaxThick = squareToolHMMaxThick.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolHSMaxDipping = squareToolHMMaxDipping.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolHSMiddle = squareToolHSMiddle.IsChecked == true;
            OptionOptimizerCNCToolsSetting.Default.squareToolHSMinDepth = squareToolHSMinDepth.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolHSNr = shapeToolHSNr.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolHSDistance = shapeToolHSDistance.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolHSMaxThick = shapeToolHMMaxThick.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolHSMaxDipping = shapeToolHMMaxDipping.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolHSMiddle = shapeToolHSMiddle.IsChecked == true;
            OptionOptimizerCNCToolsSetting.Default.squareToolDiaMinDepth = squareToolDiaMinDepth.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolHSMinDepth = shapeToolHSMinDepth.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyNr = squareToolEmergencyNr.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyDistance = squareToolEmergencyDistance.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyMaxThick = squareToolEmergencyMaxThick.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyMaxDipping = squareToolEmergencyMaxDipping.Text;
            OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyMiddle = squareToolEmergencyMiddle.IsChecked == true;
            OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyMinDepth = squareToolEmergencyMinDepth.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyNr = shapeToolEmergencyNr.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyDistance = shapeToolEmergencyDistance.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyMaxThick = shapeToolEmergencyMaxThick.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyMaxDipping = shapeToolEmergencyMaxDipping.Text;
            OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyMiddle = shapeToolEmergencyMiddle.IsChecked == true;
            OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyMinDepth = shapeToolEmergencyMinDepth.Text;
            OptionOptimizerCNCToolsSetting.Default.tb_mindLength = Convert.ToSingle(tb_mindLength.Text);
            OptionOptimizerCNCToolsSetting.Default.tb_mindWidth = Convert.ToSingle(tb_mindWidth.Text);
            OptionOptimizerCNCToolsSetting.Default.cb_doComponentBigger = cb_doComponentBigger.IsChecked == true;
            OptionOptimizerCNCToolsSetting.Default.cb_doMinEdgeDistance = cb_doMinEdgeDistance.IsChecked == true;
            OptionOptimizerCNCToolsSetting.Default.tb_minEdgeDistance = Convert.ToSingle(tb_minEdgeDistance.Text);
            OptionOptimizerCNCToolsSetting.Default.tb_minEdgeDrilling = Convert.ToSingle(tb_minEdgeDrilling.Text);
            OptionOptimizerCNCToolsSetting.Default.cb_doContourOptimiazation = cb_doContourOptimiazation.IsChecked == true;
            OptionOptimizerCNCToolsSetting.Default.Save();
            this.Close();
        }
    }
}
