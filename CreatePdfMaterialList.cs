﻿using iText.Barcodes;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Action;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Kernel.Font;
using System;
using System.Collections.Generic;
using System.Text;
using iText.IO.Font;


namespace Vectorworks_Optimizer
{
    class CreatePdfMaterialList 
    {
        private float edgeSpacing = 20f;
        private string pdfNameTemp = "";
        private string groundPath = OptionGeneralSetting.Default.rootFolderCNC;
        PdfDocument pdfDoc;
        Document doc;
        PdfFont fontBold = PdfFontFactory.CreateFont(FontConstants.HELVETICA_BOLD);
       
        public void pOpen(string path, string pdfName)
        {
            pdfNameTemp = pdfName;
           
                PdfWriter writer = new PdfWriter(path + @"\" + pdfName + ".pdf");
            
                pdfDoc = new PdfDocument(writer);
                pdfDoc.SetDefaultPageSize(PageSize.A4);
            
                doc = new Document(pdfDoc).SetFontSize(8);
                doc.SetMargins(edgeSpacing, edgeSpacing, edgeSpacing, edgeSpacing);
            

        }
        
        public void addHeaderTableMaterial(string dictionaryPath, string projektNr, string customerName)
        {
            Table table = new Table(new float[] { 1f, 2f,4f },true);
            Cell cell = new Cell().Add(new Paragraph(pdfNameTemp));
            table.AddCell(cell);
            cell = new Cell().SetHorizontalAlignment(HorizontalAlignment.RIGHT).Add(new Paragraph("Projekt-Nr: "+projektNr +"  "+customerName));
            table.AddCell(cell);
            cell = new Cell().Add(new Paragraph("OrdnerPfad"+dictionaryPath));
            table.AddCell(cell);
            doc.Add(table);
            
        }
        public void addHeaderTableMaterialInfo()
        {
           // float [] pointColumnWidths = { 50f,100f,50f,100f,100f,100f,100f,100f,100f,50f};
            float[] pointColumnWidths = { 0.5f, 3f, 1f, .3f, 2f, 2f, 1f, 0.5f };
            Table table = new Table(UnitValue.CreatePercentArray(pointColumnWidths)).UseAllAvailableWidth();
            
            Table tablePosPartInfo = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            tablePosPartInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderBottom(new SolidBorder(1)).Add(new Paragraph("Pos")));
            tablePosPartInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("BauT")));
            table.AddCell(tablePosPartInfo);
            Table tableComponentInfo = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            tableComponentInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("Möbelname/Bezeichnung")));
            tableComponentInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("Bauteilname")));
            table.AddCell(tableComponentInfo);
            table.AddCell("Material");
            table.AddCell("Stk");
            Table tableCutInfo = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            tableCutInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("Zuschnittmass")));
            tableCutInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderBottom(new SolidBorder(1)).Add(new Paragraph("Fertigmass")));
            Table tableSizeInfo = new Table(UnitValue.CreatePercentArray(3)).UseAllAvailableWidth().SetPaddingLeft(-1).SetPaddingRight(-1);
            tableSizeInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderRight(new SolidBorder(1)).Add(new Paragraph( "Länge"))); 
            tableSizeInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderRight(new SolidBorder(1)).Add(new Paragraph("Breite")));
            tableSizeInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("Dicke")));
            tableCutInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(tableSizeInfo));
            table.AddCell(tableCutInfo);
            //Table tableEdgeInfo = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            table.AddCell("KantenSkizze");
            table.AddCell("Beläge  Oberfläche");
            Table tableCutDone = new Table(1);
            tableCutDone.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderBottom(new SolidBorder(1)).Add(new Paragraph("ZS")));
            tableCutDone.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("CNC")));
            table.AddCell(tableCutDone);

            doc.Add(table);
        }
        public void AddTableMaterial(
            string ComponentNr,
            string projectNr, 
            string positionNr,
            string furniturName,
            string carrierMaterial,
            string designation,
            int count,
            float finalLength, float finalWidth, float thick,
            float cuttingLength, float cuttingWidth,
            string componentName,
            string edgeFront, string edgeBack,
            string edgeLeft, string edgeRight,
            string polygonalEdge, string existEdge,
            string surfaceOne, string surfaceTwo,
            string coating,
            string ncMain,string ncMain2, string ncNext, string ncNext2)
        {
            float[] pointColumnWidths = { 0.5f, 3f, 1f, .3f, 2f, 2f, 1f, 0.5f };
            Table table = new Table(UnitValue.CreatePercentArray(pointColumnWidths)).UseAllAvailableWidth();

            Table tablePosPartInfo = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            tablePosPartInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderBottom(new SolidBorder(1)).Add(new Paragraph(positionNr)));
            tablePosPartInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph(ComponentNr+"")));
            table.AddCell(tablePosPartInfo);
            Table tableComponentInfo = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            if (furniturName == "")
            {
                tableComponentInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph(designation)));
            }
            else
            {
                tableComponentInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph(furniturName)));
            }
            tableComponentInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph(componentName)));
            table.AddCell(tableComponentInfo);
            table.AddCell(carrierMaterial);
            table.AddCell(count+"");
            Table tableCutInfo = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();           
            Table tableSizeInfo = new Table(UnitValue.CreatePercentArray(3)).UseAllAvailableWidth();
            tableSizeInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderRight(new SolidBorder(1)).SetFont(fontBold).Add(new Paragraph(cuttingLength+"")));
            tableSizeInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderRight(new SolidBorder(1)).SetFont(fontBold).Add(new Paragraph(cuttingWidth+"")));
            tableSizeInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetFont(fontBold).Add(new Paragraph(thick+"")));            
            tableCutInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderBottom(new SolidBorder(1)).Add(tableSizeInfo));
            Table tableSizeInfo2 = new Table(UnitValue.CreatePercentArray(3)).UseAllAvailableWidth();
            tableSizeInfo2.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderRight(new SolidBorder(1)).Add(new Paragraph(finalLength+"")));
            tableSizeInfo2.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderRight(new SolidBorder(1)).Add(new Paragraph(finalWidth+"")));
            tableSizeInfo2.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph(thick + "")));
            tableCutInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(tableSizeInfo2));
            table.AddCell(tableCutInfo);
            Table tableEdgeInfo = new Table(UnitValue.CreatePercentArray( new float[] { 0.5f, 3f, 0.5f })).SetTextAlignment(TextAlignment.CENTER);
            tableEdgeInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("-"+edgeLeft).SetRotationAngle(-Math.PI/2)));
            Table tableEdgeFrontBack = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            tableEdgeFrontBack.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("-"+edgeBack).SetRotationAngle(Math.PI).SetTextAlignment(TextAlignment.CENTER)));
            tableEdgeFrontBack.AddCell("");
            tableEdgeFrontBack.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("-"+edgeFront)));
            tableEdgeInfo.AddCell(new Cell().Add(tableEdgeFrontBack).SetBorder(Border.NO_BORDER));
            tableEdgeInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("-"+edgeRight).SetRotationAngle(Math.PI/2)));
            table.AddCell(tableEdgeInfo);
            //table.AddCell(polygonalEdge);    
            Table tableSurfaceInfo = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            tableSurfaceInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderBottom(new SolidBorder(1)).Add(new Paragraph("H:"+surfaceOne)));
            tableSurfaceInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderBottom(new SolidBorder(1)).Add(new Paragraph("N:" + surfaceTwo)));
            tableSurfaceInfo.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph(coating)));
            table.AddCell(tableSurfaceInfo);
            Table tableCutDone = new Table(1);
            tableCutDone.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorderBottom(new SolidBorder(1)).Add(new Paragraph("0")));
            tableCutDone.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(new Paragraph("0")));
            table.AddCell(tableCutDone);
            
            doc.Add(table);
            if ( ncMain != "" || ncMain2 != "" || ncNext != "" || ncNext2 != "")
            {
                Table table2 = new Table(UnitValue.CreatePercentArray(new float[] { 0.5f, 1f, 0.5f, 1f, 0.5f, 1f, 0.5f, 1f })).UseAllAvailableWidth();
                table2.SetHorizontalAlignment(HorizontalAlignment.CENTER);
                //barcode128.SetCodeSet(Barcode128.CODE128);

                table2.AddCell("H1:");
                if (ncMain != "") table2.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(qrCodeGenerate(ncMain + OptionGeneralSetting.Default.ncCodeEnd)).SetBorder(new DottedBorder(0.5f)));
                else table2.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorder(new DottedBorder(0.5f)));
                table2.AddCell("H2:");
                if (ncMain2 != "") table2.AddCell(new Cell().SetBorder(Border.NO_BORDER).Add(qrCodeGenerate(ncMain2 + OptionGeneralSetting.Default.ncCodeEnd)).SetBorder(new DottedBorder(0.5f)));
                else table2.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorder(new DottedBorder(0.5f)));
                table2.AddCell("N1:");
                if (ncNext != "") table2.AddCell(new Cell().Add(qrCodeGenerate(ncNext + OptionGeneralSetting.Default.ncCodeEnd)).SetBorder(Border.NO_BORDER).SetBorder(new DottedBorder(0.5f)));
                else table2.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorder(new DottedBorder(0.5f)));
                table2.AddCell("N2:");
                if (ncNext2 != "") table2.AddCell(new Cell().Add(qrCodeGenerate(ncNext2 + OptionGeneralSetting.Default.ncCodeEnd)).SetBorder(Border.NO_BORDER).SetBorder(new DottedBorder(0.5f)));
                else table2.AddCell(new Cell().SetBorder(Border.NO_BORDER).SetBorder(new DottedBorder(0.5f)));


                doc.Add(table2);
            }
            


        }
        public void pClose()
        {
            doc.Close();
        }
        
        private Image qrCodeGenerate(string path, string projectNR)
        {
            BarcodeQRCode barcodeQRCode = new BarcodeQRCode();
            barcodeQRCode.SetCode(groundPath +@"\" + projectNR + @"\" + path);
            Image qrCodeImeage = new Image(barcodeQRCode.CreateFormXObject(pdfDoc));
            qrCodeImeage.ScaleAbsolute(30, 30);
            return qrCodeImeage;
        }
        private Image qrCodeGenerate(string NCCode)
        {
            BarcodeQRCode barcodeQRCode = new BarcodeQRCode();
            barcodeQRCode.SetCode(NCCode);
            Image qrCodeImeage = new Image(barcodeQRCode.CreateFormXObject(pdfDoc));
            qrCodeImeage.ScaleAbsolute(30, 30);
            return qrCodeImeage;
        }

    }
}