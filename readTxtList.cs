﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace Vectorworks_Optimizer
{
    class readTxtList
    {
        private string[] temp;
        private List<ConstructorMaterialList> constructorMaterialLists = new List<ConstructorMaterialList>();
        private List<ConstructorFittingList> constructorFittingLists = new List<ConstructorFittingList>();
        private List<ConstructorSurfaceList> constructorSurfaceLists = new List<ConstructorSurfaceList>();
        private List<ConstructorMaterialChanger> constructorMaterialChangers = new List<ConstructorMaterialChanger>();
        private ConstructorMaterialChanger constructorMaterialChangerVariabel = new ConstructorMaterialChanger();
        public void openRead(string path)
        {
            temp = System.IO.File.ReadAllLines(path, Encoding.UTF7);
            fillLists();
        }
        private void fillLists()
        {
            string takeMaterial=",";
            string takeEdge=",";
            string takeCoatting = ",";
            string takeSurface=",";
            bool jumpOverFirstLine = true;
            foreach (var currentLine in temp)
            {
                if (jumpOverFirstLine)
                {
                    jumpOverFirstLine = false;
                    continue;
                }

                string[] temp2 = currentLine.Split('\t');
                if (temp2[4] == "Beschlag")
                {
                    constructorFittingLists.Add(new ConstructorFittingList()
                    {
                        ProjectID = temp2[0],
                        PojectPos = temp2[1],
                        PosDescription = temp2[2],
                        FurnitureName = temp2[3],
                        ComponentDefinition = temp2[4],
                        ComponentName = temp2[5],
                        ItemsNR = temp2[6],
                        Count = Convert.ToInt32(temp2[7]),
                        CuttingLength = Convert.ToSingle(temp2[11]),
                        CustomerLastName = temp2[34],
                        ProjectName = temp2[35],
                        FileName = temp2[36]
                    });
                }
                else if (temp2[4] == "Belag")
                {
                    if(!takeSurface.Contains(temp2[6] + ",")){
                        fillMaterialchanger(2, temp2[6], Convert.ToSingle(temp2[10]), 4, 0);
                        takeSurface += temp2[6] + ",";
                    }
                    constructorSurfaceLists.Add(new ConstructorSurfaceList()
                    {
                        ComponentNumber = 0,
                        ProjectID = temp2[0],
                        PojectPos = temp2[1],
                        PosDescription = temp2[2],
                        FurnitureName = temp2[3],
                        ComponentDefinition = temp2[4],
                        ComponentName = temp2[5],
                        CarrierMaterial = temp2[6],
                        Count = Convert.ToInt32(temp2[7]),
                        Thick = temp2[10],
                        CuttingLength = Convert.ToSingle(temp2[11]),
                        CuttingWidth = Convert.ToSingle(temp2[12]),
                        Rotatable = temp2[13],
                        EdgeFront = "",
                        EdgeBack = "",
                        EdgeLeft = "",
                        EdgeRight = "",
                        NC1 = "",
                        NC2 = "",
                        CustomerLastName = temp2[34],
                        ProjectName = temp2[35],
                        FileName = temp2[36],
                        EditType = "Dia",
                        getBasicMaterialNumber = 0
                    }) ; 
                }
                else
                {
                    if (!takeMaterial.Contains(temp2[6] + ","))
                    {
                        fillMaterialchanger(0, temp2[6], Convert.ToSingle(temp2[10]), 1, 0);
                        takeMaterial += temp2[6] + ",";
                    }
                    if(temp2[24] != "")
                    {
                        string[] temp3 = temp2[24].Split(", ");
                        foreach( string tempLine in temp3)
                        {
                            if (!takeEdge.Contains(tempLine + ","))
                            {
                                fillMaterialchanger(1, tempLine, 0, 0, 1);
                                takeEdge += tempLine + ",";
                            }
                        }
                        
                    }
                    if (!takeCoatting.Contains(temp2[28] + ","))
                    {
                        fillMaterialchanger(3, temp2[28], 0, 0, 0);
                    }
                    constructorMaterialLists.Add(new ConstructorMaterialList()
                    {
                        ComponentNumber = 0,
                        ProjectID = temp2[0],
                        PojectPos = temp2[1],
                        PosDescription = temp2[2],
                        FurnitureName = temp2[3],
                        ComponentDefinition = temp2[4],
                        ComponentName = temp2[5],
                        CarrierMaterial = temp2[6],
                        Count = Convert.ToInt32(temp2[7]),
                        FinalLength = Convert.ToSingle(temp2[8]),
                        FinalWidth = Convert.ToSingle(temp2[9]),
                        Thick = Convert.ToSingle(temp2[10]),
                        CuttingLength = Convert.ToSingle(temp2[11]),
                        CuttingWidth = Convert.ToSingle(temp2[12]),
                        Rotatable = temp2[13],
                        EdgeFront = temp2[14],
                        EdgeBack = temp2[16],
                        EdgeLeft = temp2[18],
                        EdgeRight = temp2[20],
                        EdgeCode = temp2[22],
                        EdgeVN = temp2[23],
                        EdgePoly = temp2[24],
                        EdgeExist = temp2[25],
                        SurfaceH = temp2[26],
                        SurfaceN = temp2[27],
                        Coating = temp2[28],
                        NCMain = temp2[31],
                        NCMain2 = "",
                        NCNext = temp2[32],
                        NCNext2 = "",
                        NCForming = temp2[33],
                        CustomerLastName = temp2[34],
                        ProjectName = temp2[35],
                        FileName = temp2[36],
                        EditType = "Dia",
                        EdgeBevorSurface = false

                    }) ;
                }

            }
        }
        public void fillMaterialchanger(int category,string carrierMaterial,float thick,int type,int typeEdge) 
        {
            string tempFormat = "";
            if (category == 0) tempFormat = constructorMaterialChangerVariabel.formatArray[1];
            if (category == 2) tempFormat = constructorMaterialChangerVariabel.formatArray[6];
            
            constructorMaterialChangers.Add(new ConstructorMaterialChanger()
            {
                Category = constructorMaterialChangerVariabel.categoryType[category],
                CarrierMaterial = carrierMaterial,
                Thick = thick + "",
                NewCarrierMaterial = "",
                TypeMaterial = constructorMaterialChangerVariabel.typeMaterialArray[type,1]+ 
                               constructorMaterialChangerVariabel.typeEdgeType[typeEdge,0],
                EditType = "Dia",
                Format = tempFormat,
            }) ;
        }
    
        public List<ConstructorMaterialList> getMaterialList()
        {
            return constructorMaterialLists;
        }
        public List<ConstructorFittingList> getFittingList()
        {
            return constructorFittingLists;
        }
        public List<ConstructorSurfaceList> getSurfaceList()
        {
            return constructorSurfaceLists;
        }
        public List<ConstructorMaterialChanger> getMaterialChangeList()
        {
            return constructorMaterialChangers;
        }
    }
}
