<!-- Headings -->
# Vectorworks Optimizer
## Was optimiert es
Mein Optimierer erweitert für die Produktion folgende Punkte:
* Einfacher Materialwechsler 
  * mit Bearbeitungszugehörigkeit für CNC 
  * merkt sich die Einstellungen der Materialien für den Auftrag
* Aus der Zuschnittsliste werden 4 PDF generiert 
  * Werkstoffliste mit Kantenbild und QR-Code
  * Belagsliste mit Zusammengehöhrigkeit zum Trägermaterial
  * Beschlagsliste mit Anzahl länge und Korpuszugehörigkeit
  * Ettikettenliste für Bauteile einfach zu Beschriften + QR-Code für Scanner
* 2 Listen für das Plattenoptimierungsprogramm
  * Rohmaterialplatten
  * Materialliste mit Beachtung der Laufrichtung
* CNC Woodflash Verbesserung
  * Wechselt umfahrungswerkzeug nach Material
  * Wiederholung der Umfahrung bei zu dicken Platten
  * Ekrennung der Kanten für 2 Stufen Verfahren (Bsp: Erkennt Bohrung auf Kante und macht ein zweites CNC-Programme, um Bearbeitung, nach dem Kante anbringen, auszuführen)
  * Fräser Einfahrungsrichtung immer von aussen nach innen

## Programm Ablauf
Ist alles eingestellt, ist nur der obere Teil wichtig.

![MainWindows](https://gitlab.com/feiler/vectorworks-optimizer/-/raw/master/documentation/mainWindows.PNG)

Beispieldokumente für Produktion

![ExamplePdf](https://gitlab.com/feiler/vectorworks-optimizer/-/raw/master/documentation/examplePDF.PNG)

## Programm Installation und Einstellung
Auf meinem Gitlab-Konto können die Source-Codes und Zipdatei herungergeladen werden
[Donwloadlink Vectorworks Optimizer.zip](https://gitlab.com/feiler/vectorworks-optimizer/-/raw/master/Vectorworks%20Optimizer.zip)
Diese könnt ihr entpacken wohin ihr auch wollt. Im Geschäft hab ich auf das Nas gepackt, um es für alle einfacher zu ersetzen.

Nun startet ihr die "Vectorworks optimizer.exe" am besten macht ihr eine Veknüpfung mit dem Desktop
![ExampleDictionaryExe](https://gitlab.com/feiler/vectorworks-optimizer/-/raw/master/documentation/examplePDF.PNG)

Geht in die "Einstellungen" und unter Stamm CNC Ordner und gebt an, wo ihr die CNC-Daten speichert. Im Geschäft ist es auf dem Nas, worauf die CNC direkt zugriff hat, um über QR die Programme zu laden.
Nutzt ihr die QR-Codes sollte der PFad nicht mehr als 5 Zeichen lang sein.

Beispiel Geschäft -> N:\C
Im Bild ist es auf meinem Lokalem Rechner. Der Rest muss nicht beachtet werden, da es zur Zeit tod ist.

![Option](https://gitlab.com/feiler/vectorworks-optimizer/-/raw/master/documentation/option.PNG)

auf Speicher und weiter gehts

Habt ihr Woodflash könnt ihr in "Einstellungen CNC Optimierung" gehen und das Fenster gross machen, um es richtig anzusehen.
Beispiel wie es eingestellt werden kann:

![OptionCNC](https://gitlab.com/feiler/vectorworks-optimizer/-/raw/master/documentation/optionCNC.PNG)

Als letztes muss die Zuschnittliste von mir in Vectorworks gebracht werden. Nur so kann leider alles genutzt werden. Öffnet "Installation der Zuschnittliste in Vectorworks".

![InstallCuttinglist](https://gitlab.com/feiler/vectorworks-optimizer/-/raw/master/documentation/installCuttinglist.PNG)

Nun ist Vectorworks dran

![VwOption](https://gitlab.com/feiler/vectorworks-optimizer/-/raw/master/documentation/vwAusgabe.PNG)

Einstellung der Zuschnittliste sollte genau so aussehen.
![VwCuttinglist](https://gitlab.com/feiler/vectorworks-optimizer/-/raw/master/documentation/vwStueckliste.PNG)

## Erklärung zum CNC-Ordner Aufbau
Damit die CNC die Programme direkt vom Nas/Server lesen kann muss bei Woodflash leider die Zeichencode länge möglichst kurz gehalten werden.

Nach dem Umwandeln sollte es so aussehen.
![AfterConverting](https://gitlab.com/feiler/vectorworks-optimizer/-/raw/master/documentation/afterConverting.PNG)

## Erklärung zur Erstellung der QR-Codes
Der generierte Pfad, welcher in den QR-Codes speichern, ist der bei der Erstellung durch das Program. Das bringt folgene Vorteile:
* Der Pfad stimmt ganz sicher
* Die Maschinencodes werden vom NAS/SERVER geholt und müssen nicht auf die Maschine
* Die Zeichenlänge wird nicht überschritten, da die Namenslänge durchs Program angepasst sind
## Was wird beim CNC Optimiert
* Dickere Platten können in mehreren Stufen umfahren werden und die Ausriese können verhindert werden
* Wechselt Umfahrunsfräser bei Typ (Spannplatte/Massivholz) und wenn es kein Rechteck ist.
* Gehrungsschnitte welche über eine ABS-Kante gehen, werden ist beim 2ten CNC Programm gemacht.
* Bohrungen in ABS-Kante, werden ist beim 2ten CNC Programm gemacht.
* Grosse Löcher für den Kantenleimer problematisch sind, werden ist beim 2ten CNC Programm gemacht.
* Konturen, welche nach aussen über die ABS-Kante gehen, werden ist beim 2ten CNC Programm gemacht und so angepasst, dass diese nun nach innen Fräsen
* Gibt an die Ettiketten die Anschlagsseite, damit der Maschinist weiss, welche Seite zu Ihm schaut
  
## Inhalt Werkstoffliste
* Position + Bauteilnummer
* Möbelname + Bauteilname
* Material
* Stückzahl
* Zuschnitt+Fertigmass
* Kantenbild
* Belag Oberfläche
* Möglichkeit Zuschnitt und CNC bearbeitet abzuhacken
* QR-Code für Scanner nutzen
## Inhalt Belag
* Position + Bauteilnummer
* Belagsname + Bauteilzugehörigkeit
* Aufklebseite
* Material
* Stückzahl
* Zuschnitt
* Möglichkeit Zuschnitt und CNC bearbeitet abzuhacken
* QR-Code für Scanner nutzen

## Inhalt Ettiketten
[Link zu den verwendeten Ettikettengrösse](https://www.fruugoschweiz.com/avery-zweckform-l6011-20-etiketten-635-x-296-mm-polyester-film-silber-540-pc-permanent-namensschilder-laser-kopierer/p-5690052-12719377?language=de)
* Kantenbild (Rechtecke) + Kantenreihenfolge + Seitenlänge für Kleberorientierung + Anschlagseite auf Maschine
* Nutzt Auftragsnummer + Kundennamen
* Bauteilnummer
* Zugehörigkeit
* Bauteilname
* Laufrichtung, wenn im Material hinterlegt wurde(Zeichnungsprogrammseitig)
* Belagsettiketten Seitendefiniert
* QR-Codes für CNC

## Inhalt Beschlagsliste
* Position
* Zugehörigkeit bei Möbel 100% / Bauteilen nur Teilsmöglich da VW keine weiteren Infos kann geben
* Artikelnummer
* Artikelbeschreibung
* Anzahl
* Länge falls vorhanden
* Feld Bestellt und da zum abkreuzen

## Verwendete Zuschnittsliste
Für den Betrieb habe ich OptiCut organisiert, weil es Flexibel und günstig war. Früher hatten wir die Ettikettenfunktion davon noch genutzt. Die hab ich nun selber gemacht, da ich mehr rausholen kann.
Vielleicht schreib ich mal eine eigene Plattenoptimierung (Klingt nach einer herausforderung :-) )
## Zukünftige Pläne
* [] 2te CNC- Bearbeitungen nur noch die Problembearbeitungen. Also hält nur die Bearbeitungen zurück, welche besser nach dem Kantenanbringen sind.
* [] Aufmerksamkeitsliste/ Kontrollliste (Bsp: Keine Pufferlöcher auf Tür oder Bauteil zu gross für CNC)
* [] Möglichkeit ohne Bauteilnummerierung zu Arbeiten, damit bei zusätzlichen Teilen die Kleber nicht umsonst sind + erweiterte Strategie für Veränderungen
* [] zu schmale Bauteile verbreitern oder mit einem andern kompinieren
* [] Kantenbild bei nicht Rechteckigenteilen nutzen (Schwierig da noch kein System hinter der Nummerierung entdeckt)
* [] Materialwechsler Detail Ausbesserung
* [] Möglichkeit Bauteile aus dem CNC Kreislauf heruaszunehmen (Bsp: einfacher Fachboden/Tablar)
* [] Programm absturz bei hintereinander Listengenerierungsbefehl
* [] Zeichendreieck noch funktioniertent einbauen (Wird voraussichtlich nur bei Korpusteilen funktionieren)
* [] System für Einleimer überlegen und einbauen
* [] Kantenreihenfolge ersichtlicher machen
* [] Einfache Listen, welche sich bei Koch und Häfele einlesen lassen (Voraussichtlich immer 2 Listen mit allem darauf, da es wohl nicht möglich ist Lieferanten auf die Werkstofflisten zu übergeben)
* [] Einstellungen vom Programm ins Programm packen und nicht auf den Nutzer (noch selber erlernen, was die beste Lösung wäre)
* [] Öffne Ordner nach Erstellung der Listen
* [] Programabstutz wenn PDF nicht überschieben werden kann
* [] Möglichkeit QR-Code abzustellen (Flexibilität erhöhen)
* [] Umfahrunsstrategie für Material, das nicht mehr geschliefen werden muss (Gedankenspiel)
* [] allgemeint script aufräumen



## Verwendete
* Xaml
* C#
* itext7 (Pdf-generierer und solange source-code frei einsehbar ist auch gratis)




