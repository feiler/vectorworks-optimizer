﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
namespace Vectorworks_Optimizer
{
    class CreateOptimizationListMaterial
    {
        public void writeOptimizationListFile(string path,string namePdfAddition, List<ConstructorMaterialList> constructorMaterialLists, List<ConstructorSurfaceList> constructorSurfaceLists)
        {
            using (StreamWriter sw = new StreamWriter(path + @"\" + namePdfAddition+"OtimierungMaterial.txt"))
            {
                int componentNr = 1;
                sw.WriteLine("FirstLine" + '\t' +
                              "Name" + '\t' +
                              "Anzahl" + '\t' +
                              "Länge" + '\t' +
                              "Breite" + '\t' +
                              "drehbar" + '\t' +
                              "Material" + '\t' +
                              "Dicke" + '\t' +
                              "" + '\t' +"" + '\t' +"" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' +
                              "Oberfläche" + '\t' +
                              "" + '\t' +
                              "leer" + '\t' +
                              "Infos" + '\t' +
                              "Auftrag_Pos");
                foreach (var currentLine in constructorMaterialLists)
                {
                    sw.WriteLine("BauT"+componentNr + '\t' +
                                 currentLine.ComponentName + '\t' +
                                 currentLine.Count + '\t' +
                                 currentLine.CuttingLength + '\t' +
                                 currentLine.CuttingWidth + '\t' +
                                 currentLine.Rotatable.Replace("yes","x").Replace("no","") + '\t' +
                                 currentLine.CarrierMaterial + '\t' +
                                 currentLine.Thick + '\t' +
                                 "" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' +
                                 "" + '\t' +
                                 "" + '\t' +
                                 "" + '\t' +
                                 "" + '\t' +
                                 currentLine.ProjectID+"|"+currentLine.PojectPos);
                    componentNr++;
                }
                componentNr = 1;
                foreach (var currentLine in constructorSurfaceLists)
                {
                    sw.WriteLine("BelT" + componentNr + '\t' +
                                 currentLine.ComponentName + '\t' +
                                 currentLine.Count + '\t' +
                                 currentLine.CuttingLength + '\t' +
                                 currentLine.CuttingWidth + '\t' +
                                 currentLine.Rotatable.Replace("yes", "x").Replace("no", "") + '\t' +
                                 currentLine.CarrierMaterial + '\t' +
                                 currentLine.Thick + '\t' +
                                 "" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' + "" + '\t' +
                                 "" + '\t' +
                                 "" + '\t' +
                                 "" + '\t' +
                                 "" + '\t' +
                                 currentLine.ProjectID + "|" + currentLine.PojectPos);
                    componentNr++;
                }
            }
        }
    }
}
