﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;

namespace Vectorworks_Optimizer
{
    /// <summary>
    /// Interaction logic for InstallCuttingList.xaml
    /// </summary>
    public partial class InstallCuttingList : Window
    {
        private string searchTargetDictionairy = "cuttinglistexport";
        private string directoryCuttinglist = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)+ @"\cuttingList\";
        public InstallCuttingList()
        {
            InitializeComponent();
        }

        private void button_AutomaticInstall_Click(object sender, RoutedEventArgs e)
        {
            string searchKey1 = "program*";
            string searchKey2 = "Vectorworks*";
            string searchKey3 = "cuttinglistexport";

            FileInfo[] fileInfos = new DirectoryInfo(directoryCuttinglist).GetFiles();

            DirectoryInfo directoryInfo = new DirectoryInfo(@"C:\");
            DirectoryInfo[] directoriesArray1 = directoryInfo.GetDirectories(searchKey1, SearchOption.TopDirectoryOnly);

            foreach(DirectoryInfo directoryInfoElement1 in directoriesArray1)
            {                
                DirectoryInfo[] directoriesArray2 = directoryInfoElement1.GetDirectories(searchKey2, SearchOption.TopDirectoryOnly);
                foreach (DirectoryInfo directoryInfoElement2 in directoriesArray2)
                {
                    DirectoryInfo[] directoriesArray3 = directoryInfoElement2.GetDirectories(searchKey3, SearchOption.AllDirectories);
                    foreach (DirectoryInfo directoryInfoElement3 in directoriesArray3)
                    {
                        if(directoryInfoElement3.FullName.Contains(@"exportstarter\cuttinglistexport"))
                        {
                            Process.Start("explorer.exe", directoryInfoElement3.FullName);
                        }    
                       
                    }
                }
            }
            Process.Start("explorer.exe", directoryCuttinglist);
        }

        private void button_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
