﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Vectorworks_Optimizer
{
    /// <summary>
    /// Interaction logic for MaterialChanger.xaml
    /// </summary>
    public partial class MaterialChanger : Window
    {
        private string fileNameChangeMateriallist = "Material_Wechsel_Liste.txt";
        private string path;
        private int highField = 20;
        private ConstructorMaterialChanger constructorMaterialChangerArrays = new ConstructorMaterialChanger();
        private List<ConstructorMaterialChanger> consistingConstructorMaterialChangers = new List<ConstructorMaterialChanger>();
        private List<ConstructorMaterialChanger> constructorMaterialChangers = new List<ConstructorMaterialChanger>();
        
        public MaterialChanger()
        {
            InitializeComponent();

        }
        public void openMaterialChanger(List<ConstructorMaterialChanger> constructorMaterialChanger, string pathChangeMaterialList)
        {
            constructorMaterialChangers = constructorMaterialChanger;
            path = pathChangeMaterialList;
            if (File.Exists(pathChangeMaterialList + @"\" + fileNameChangeMateriallist))
            {
                loadConsistingList(pathChangeMaterialList + @"\" + fileNameChangeMateriallist);
                MateriallistMatching();

            }
            fillMaterialChanger();


        }
        public void fillMaterialChanger()
        {
            int counterField = 1;
            foreach (var currentLine in constructorMaterialChangers)
            {
                TextBlock tb_CarrierMaterial = new TextBlock();
                TextBlock tb_Thick = new TextBlock();
                TextBox tb_CarrierMaterialNew = new TextBox();
                ComboBox cb_Type = new ComboBox();
                ComboBox cb_EditType = new ComboBox();
                ComboBox cb_Format = new ComboBox();

                tb_CarrierMaterial.Text = currentLine.CarrierMaterial;
                tb_CarrierMaterial.Height = highField;
                tb_CarrierMaterial.Name = "field" + counterField + "_1";
                RegisterName(tb_CarrierMaterial.Name, tb_CarrierMaterial);
               
                tb_Thick.Text = currentLine.Thick;
                tb_Thick.Height = highField;
                tb_Thick.Name = "field" + counterField + "_2";
                RegisterName(tb_Thick.Name, tb_Thick);
               
                tb_CarrierMaterialNew.Text = currentLine.NewCarrierMaterial;
                tb_CarrierMaterialNew.Height = highField;
                tb_CarrierMaterialNew.Name = "field" + counterField + "_3";
                RegisterName(tb_CarrierMaterialNew.Name, tb_CarrierMaterialNew);
               
                foreach (string addLine in constructorMaterialChangerArrays.editTypeArray)
                {
                    cb_EditType.Items.Add(addLine);
                }
                cb_EditType.Height = highField;
                cb_EditType.SelectedIndex = cb_EditType.Items.IndexOf(currentLine.EditType);
                cb_EditType.Name = "field" + counterField + "_5";
                RegisterName(cb_EditType.Name, cb_EditType);
               
                foreach (string addLine in constructorMaterialChangerArrays.formatArray)
                {
                    cb_Format.Items.Add(addLine);
                }
                cb_Format.Height = highField;
                cb_Format.SelectedIndex = cb_Format.Items.IndexOf(currentLine.Format);
                cb_Format.Name = "field" + counterField + "_6";
                RegisterName(cb_Format.Name, cb_Format);
               
               cb_Type.Height = highField;

                if (currentLine.Category == constructorMaterialChangerArrays.categoryType[0])
                {
                    for (int i = 0; i < constructorMaterialChangerArrays.typeMaterialArray.GetLength(0); i++)
                    {
                        cb_Type.Items.Add(constructorMaterialChangerArrays.typeMaterialArray[i, 1]);
                    }
                    cb_Type.SelectedIndex = cb_Type.Items.IndexOf(currentLine.TypeMaterial);
                    cb_Type.Name = "field" + counterField + "_4";
                    RegisterName(cb_Type.Name, cb_Type);

                    loadPlatesCarriers.Children.Add(tb_CarrierMaterial);
                    loadPlatesThick.Children.Add(tb_Thick);
                    loadPlatesCarriersNew.Children.Add(tb_CarrierMaterialNew);
                    loadPlatesEdit.Children.Add(cb_EditType);
                    loadPlatesFormat.Children.Add(cb_Format);
                    loadPlatesMatType.Children.Add(cb_Type);
                }
                else if (currentLine.Category == constructorMaterialChangerArrays.categoryType[1])
                {
                    for (int i = 0; i < constructorMaterialChangerArrays.typeEdgeType.GetLength(0); i++)
                    {
                        cb_Type.Items.Add(constructorMaterialChangerArrays.typeEdgeType[i, 0]);
                    }
                    cb_Type.SelectedIndex = cb_Type.Items.IndexOf(currentLine.TypeMaterial);
                    cb_Type.Name = "field" + counterField + "_4";
                    RegisterName(cb_Type.Name, cb_Type);

                    loadEdgeCarriers.Children.Add(tb_CarrierMaterial);
                    loadEdgeThick.Children.Add(tb_Thick);
                    loadEdgeCarriersNew.Children.Add(tb_CarrierMaterialNew);
                    loadEdgeEdit.Children.Add(cb_EditType);
                    loadEdgeFormat.Children.Add(cb_Format);
                    loadEdgeMatType.Children.Add(cb_Type);

                }
                else if (currentLine.Category == constructorMaterialChangerArrays.categoryType[2])
                {
                    for (int i = 0; i < constructorMaterialChangerArrays.typeSurfaceType.GetLength(0); i++)
                    {
                        cb_Type.Items.Add(constructorMaterialChangerArrays.typeSurfaceType[i, 0]);
                    }
                    cb_Type.SelectedIndex = cb_Type.Items.IndexOf(currentLine.TypeMaterial);
                    cb_Type.Name = "field" + counterField + "_4";
                    RegisterName(cb_Type.Name, cb_Type);

                    loadSurfaceCarriers.Children.Add(tb_CarrierMaterial);
                    loadSurfaceThick.Children.Add(tb_Thick);
                    loadSurfaceCarriersNew.Children.Add(tb_CarrierMaterialNew);
                    loadSurfaceEdit.Children.Add(cb_EditType);
                    loadSurfaceFormat.Children.Add(cb_Format);
                    loadSurfaceMatType.Children.Add(cb_Type);

                }
                else if (currentLine.Category == constructorMaterialChangerArrays.categoryType[3])
                {
                    cb_Type.Items.Add("");
                    
                    cb_Type.SelectedIndex = 0;
                    cb_Type.Name = "field" + counterField + "_4";
                    RegisterName(cb_Type.Name, cb_Type);

                    loadCoatingCarriers.Children.Add(tb_CarrierMaterial);
                    loadCoatingThick.Children.Add(tb_Thick);
                    loadCoatingCarriersNew.Children.Add(tb_CarrierMaterialNew);
                    loadCoatingEdit.Children.Add(cb_EditType);
                    loadCoatingFormat.Children.Add(cb_Format);
                    loadCoatingMatType.Children.Add(cb_Type);

                }
                counterField++;

            }
        }
        

        public void MateriallistMatching()
        {
            foreach (var currentLine in constructorMaterialChangers)
            {
                foreach (var currentLine2 in consistingConstructorMaterialChangers)
                {
                    if (currentLine.Category == currentLine2.Category &&
                        currentLine.CarrierMaterial == currentLine2.CarrierMaterial &&
                        currentLine.Thick == currentLine2.Thick)
                    {
                        currentLine.NewCarrierMaterial = currentLine2.NewCarrierMaterial;
                        currentLine.TypeMaterial = currentLine2.TypeMaterial;
                        currentLine.EditType = currentLine2.EditType;
                        currentLine.Format = currentLine2.Format;
                        break;
                    }
                }
            }
        }
        public void loadConsistingList(string path)
        {
            string[] temp = File.ReadAllLines(path, Encoding.UTF8);
            foreach (string currentLine in temp)
            {
                string[] temp2 = currentLine.Split('\t');

                consistingConstructorMaterialChangers.Add(new ConstructorMaterialChanger()
                {
                    Category = temp2[0],
                    CarrierMaterial = temp2[1],
                    Thick = temp2[2],
                    NewCarrierMaterial = temp2[3],
                    TypeMaterial = temp2[4],
                    EditType = temp2[5],
                    Format = temp2[6],
                });
            }

        }
        public List<ConstructorMaterialChanger> getNewMaterialChanger()
        {
            return constructorMaterialChangers;
        }

        private void bt_SaveAndClose_Click(object sender, RoutedEventArgs e)
        {
            int counterField = 1;
            List<string> matChangeList = new List<string>();
           
            foreach(var currentLine in constructorMaterialChangers)
            {
                TextBlock tb_CarrierMaterial = this.FindName("field" + counterField + "_1") as TextBlock;
                TextBlock tb_Thick = this.FindName("field" + counterField + "_2") as TextBlock;
                TextBox tb_CarrierMaterialNew = this.FindName("field" + counterField + "_3") as TextBox;
                ComboBox cb_Type = this.FindName("field" + counterField + "_4") as ComboBox;
                ComboBox cb_EditType = this.FindName("field" + counterField + "_5") as ComboBox;
                ComboBox cb_Format = this.FindName("field" + counterField + "_6") as ComboBox;
                                
                if (currentLine.CarrierMaterial == tb_CarrierMaterial.Text)
                {
                    currentLine.NewCarrierMaterial = tb_CarrierMaterialNew.Text;
                    currentLine.TypeMaterial = cb_Type.Text;
                    currentLine.EditType = cb_EditType.Text;
                    currentLine.Format = cb_Format.Text;
                }
                counterField++;
                    matChangeList.Add(currentLine.Category + '\t'+
                        currentLine.CarrierMaterial + '\t' +
                        currentLine.Thick + '\t' +
                        currentLine.NewCarrierMaterial + '\t' +
                        currentLine.TypeMaterial + '\t' +
                        currentLine.EditType + '\t' +
                        currentLine.Format);
            }
            foreach(var currentLine in consistingConstructorMaterialChangers)
            {
                
                for(int i = 0; i < constructorMaterialChangers.Count; i++) 
                {
                   if( currentLine.CarrierMaterial ==constructorMaterialChangers[i].CarrierMaterial &&
                        currentLine.Thick == constructorMaterialChangers[i].Thick)
                    {
                        break;
                    }
                   if(i== constructorMaterialChangers.Count - 1)
                    {
                        matChangeList.Add(currentLine.Category + '\t' +
                        currentLine.CarrierMaterial + '\t' +
                        currentLine.Thick + '\t' +
                        currentLine.NewCarrierMaterial + '\t' +
                        currentLine.TypeMaterial + '\t' +
                        currentLine.EditType + '\t' +
                        currentLine.Format);
                    }
                }
            }
            File.WriteAllLines(path + @"\" + fileNameChangeMateriallist, matChangeList, Encoding.UTF8);
            this.Close();
        }
    }
}
