﻿using System;
using System.Collections.Generic;
using System.Text;
using CsvHelper;
using System.Globalization;
using CsvHelper.Configuration.Attributes;

namespace Vectorworks_Optimizer
{
    class MaterialList
    {        
        public int ProjektNr { get; set; }       
        public string PositionNr { get; set; }        
        public string FurnitureName { get; set; }        
        public string CarrierMaterial { get; set; }     
        public string Designation { get; set; }        
        public float Lenght { get; set; }       
        public float Width { get; set; }        
        public float Thick { get; set; }    
        public int Count { get; set; }       
        public string ComponentName { get; set; }       
        public string PolygonalEdge { get; set; }       
        public string ExistEdge { get; set; }      
        public string SurfaceOne { get; set; }    
        public string SurfaceTwo { get; set; }   
        public string Coating { get; set; } 
        public string CNCFormatting { get; set; }
        public string CNCMain { get; set; }     
        public string CNCNext { get; set; }
    }
}
