﻿using System;
using System.Collections.Generic;
using System.Text;
using iText.Barcodes;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Action;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;

namespace Vectorworks_Optimizer
{
    class CreatePdfFittingList
    {
        private float edgeSpacing = 20f;
        PdfDocument pdfDoc;
        Document doc;

        public void pOpen(string path, string pdfName)
        {

            PdfWriter writer = new PdfWriter(path + @"\" + pdfName + ".pdf");
            pdfDoc = new PdfDocument(writer);
            pdfDoc.SetDefaultPageSize(PageSize.A4);

            doc = new Document(pdfDoc).SetFontSize(8);
            doc.SetMargins(edgeSpacing, edgeSpacing, edgeSpacing, edgeSpacing);


        }
        public void addHeaderTableMaterial(string projectName, string projectNR, string customerName)
        {
            Table table = new Table(2, true);

            Cell cell = new Cell().SetFontSize(12).Add(new Paragraph("Projekt-Nr: " + projectNR +"  "+customerName));
            table.AddCell(cell);
            cell = new Cell().SetFontSize(12).Add(new Paragraph("Projektname: " + projectName));
            table.AddCell(cell);
           
            doc.Add(table);

        }
         public void addHeaderTableMaterialInfo()
        {
           // float [] pointColumnWidths = { 50f,100f,50f,100f,100f,100f,100f,100f,100f,50f};
            float[] pointColumnWidths = { 0.5f,1.5f, 1.5f, 3f, .3f, 1f, 0.5f, 0.5f};
            Table table = new Table(UnitValue.CreatePercentArray(pointColumnWidths)).UseAllAvailableWidth();

            table.AddCell("Pos");
            table.AddCell("Zugehörigkeit");
            table.AddCell("ArtikelNr");
            table.AddCell("Bezeichnung");
            table.AddCell("Stk");
            table.AddCell("Länge");
            table.AddCell("Bestellt");
            table.AddCell("da");

            doc.Add(table);
        }
        public void AddTableMaterial(string positionNr, string posDescription,
            string furniturName, string componentDefinition, string componentName,
            string itemNr,
            int count, float cuttinglength
           )
        {
            float[] pointColumnWidths = { 0.5f,1.5f, 1.5f, 3f, .3f, 1f, 0.5f, 0.5f };
            Table table = new Table(UnitValue.CreatePercentArray(pointColumnWidths)).UseAllAvailableWidth();

            table.AddCell(positionNr);
            table.AddCell(furniturName);
            if (itemNr != "") {
                table.AddCell(itemNr);
                }
            else
            {
                table.AddCell("->");
            }
            table.AddCell(componentName);
            table.AddCell(count + "");
            if (cuttinglength != 0)
            {
                table.AddCell(cuttinglength + "");
            }
            else
            {
                table.AddCell("-");
            }
            table.AddCell("");
            table.AddCell("");
            doc.Add(table);
        }
            public void pClose()
        {
            doc.Close();
        }
    }
}
