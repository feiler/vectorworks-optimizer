﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Vectorworks_Optimizer
{
    /// <summary>
    /// Interaction logic for NamePdfList.xaml
    /// </summary>
    public partial class NamePdfList : Window
    {
        public NamePdfList()
        {
            InitializeComponent();
			
        }
		private void btnDialogOk_Click(object sender, RoutedEventArgs e)
		{
			this.DialogResult = true;
		}
		
		public string getPdfAdditionName()
		{
			if (tb_pdfName.Text == "")
			{
				return "";
			}
			else
			{
				return tb_pdfName.Text;
			}
			
		}
		
	}
}
