﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Vectorworks_Optimizer
{
    class CreateOptimizationListRolling
    {
        public void writeOptimizationRollingFile(string Path,string namePdfAddition, List<ConstructorMaterialChanger> constructorMaterialChangers)
        {
            using (StreamWriter sw = new StreamWriter(Path + @"\"+ namePdfAddition+"OtimierungRolling.txt"))
            {
                int Art_Nr = 1;
                foreach (var currentLine in constructorMaterialChangers)
                {
                    if (currentLine.Category == "Kante" || 
                        currentLine.Category == "Oberfläche"||
                        currentLine.TypeMaterial== "Massiv" ||
                        currentLine.TypeMaterial == "Glass" ||
                        currentLine.TypeMaterial == "Hilfsmaterial" ) continue;
                    string name = currentLine.CarrierMaterial;
                    string materialName = currentLine.CarrierMaterial;
                    string[] size = currentLine.Format.Split("x");
                    string colorDefinition = "";
                    if (currentLine.NewCarrierMaterial != "")
                    {
                        name = currentLine.NewCarrierMaterial;
                        materialName = currentLine.NewCarrierMaterial +" "+ currentLine.Thick;
                    }
                    if (currentLine.Category == "Platte") colorDefinition = "red";
                    else if (currentLine.Category == "Belag") colorDefinition = "blue";


                    sw.WriteLine("100" + '\t' +
                                  name + '\t' +
                                  size[0] + '\t' +
                                  size[1] + '\t' +
                                  colorDefinition + '\t' +
                                  materialName + '\t' +
                                  "Nr-"+Art_Nr);
                    Art_Nr++;
                }
            };
        }
    }
}
