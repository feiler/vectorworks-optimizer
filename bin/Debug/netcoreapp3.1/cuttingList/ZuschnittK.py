# -*- coding: utf-8 -*-

# Copyright 2011-2014, extragroup GmbH, Pottkamp 19, 48149 Münster, Germany
# All rights reserved
#
# All information contained herein is, and remains the property of
# extragroup GmbH and its suppliers, if any. The intellectual and
# technical concepts contained herein are proprietary to extragroup GmbH
# and its suppliers and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material is
# strictly forbidden unless prior written permission is obtained from
# extragroup GmbH

from interiorcad.cuttinglistexport.csvbase import CuttinglistExportCSVBase
import logging
#import interiorcad.backend.exportsettingsfile
import interiorcad.interface.configurationkeys as ConfigurationKeys
from interiorcad.interface.geometry.edges import EdgeData
import re

from interiorcad.backend.localization import getLocalizedString, CUTTINGLISTEXPORT_MESSAGES
from interiorcad.backend.localization import getLocalizedString, CUTTINGLIST_RESOURCEGROUP, getConstructionGroupName, getConstructionGroupComponentName
class ZuschnittK(CuttinglistExportCSVBase):

	def __init__(self, **keywords):
		super().__init__(**keywords)

		self.EXTENSION = 'txt'
		self.EXPORT_LOCATION = self.getCuttinglistConfiguration().get('export-location', 'file')  # | clipboard
		self.SCOPE_OF_EXPORT = self.getCuttinglistConfiguration().get('scope-of-export',
																	  'partsonly')  # | partsandfittings | fittingsonly
		self.CUMULATE_COVERINGS = self.getCuttinglistConfiguration().getAsBool('cumulate-coverings', False)
		self.CUMULATE_FITTINGS = self.getCuttinglistConfiguration().getAsBool('cumulate-fittings', True)
		self.CUMULATE_WORKPIECES = self.getCuttinglistConfiguration().get('cumulate-workpieces', 'none')
		self.NC_PROGRAM_DISPLAY_NAME_DEFINITION = self.getCuttinglistConfiguration().get('nc-program-display-name-definition', 'namewithextension')
		self.sortierung = self.CONFIGURED_SORTING_CRITERIA = self.cuttinglistConfiguration.get('sorting-criteria',self.getDefaultSortingCriteria(**keywords) if hasattr(self,'getDefaultSortingCriteria') else self.DEFAULT_SORTING_CRITERIA)




		self.csvdelimiter = '\t'
		
		self.EXPORT_FORMAT = 'ZuschnittK'
		self.SpalteB = self.getCuttinglistConfiguration().get('SpalteB', 'PosNr')
		self.SpalteD = self.getCuttinglistConfiguration().get('SpalteD', 'BName')
		self.SpalteE = self.getCuttinglistConfiguration().get('SpalteE', 'Moebel')
		self.SpalteV = self.getCuttinglistConfiguration().get('SpalteV', 'leer')
		self.SpalteVKey = self.getCuttinglistConfiguration().get('SpalteVKey', '')

		if self.SpalteB == 'PosNr':
			self.CONFIGURED_SORTING_CRITERIA = self.POSITION_SORTING_CRITERIA


		self.counter = 0
		#self.export_process = '_'
		self.KanteVN = 'N'
		self.KaDict = {'Unknown': ['0','-'], '': ['0','-'], 'Continuous': ['2',self.KanteVN], 'Discontinuous': ['1',self.KanteVN], 'Miter': ['3',self.KanteVN]}

	def getJointAsString(self, jointId):
		if jointId == EdgeData.JOINT_MITER:
			return 'Miter'
		elif jointId == EdgeData.JOINT_CONTINUOUS:
			return 'Continuous'
		elif jointId == EdgeData.JOINT_DISCONTINUOUS:
			return 'Discontinuous'
		elif not jointId or jointId == EdgeData.JOINT_NOEDGE:
			return ''
		else:
			return 'Unknown'

	# Eigen
	def getFinishedSizeGrainDirection(self,**keywords):
		entry =keywords['entry']

		length = entry.workpiece.finishedSizeBounds.getLength()
		width = entry.workpiece.finishedSizeBounds.getWidth()
		if entry.grain == self.GRAIN_CROSSWISE:
			return width, length
		else:
			return length, width

	def getSimplifiedGrain(self, grain):
		if grain == self.GRAIN_NONE:
			return 'yes'
		else:
			return 'no'

	def doCuttinglistExport(self, **keywords):
		if self.EXPORT_LOCATION == 'clipboard':
			self.doClipboardExport(**keywords)
		elif self.EXPORT_LOCATION == 'fileandclipboard':
			self.doFileExport(**keywords)
			self.doClipboardExport(**keywords)
		else:
			self.doFileExport(**keywords)

	def openExportPath(self, **keywords):
		if 'file' in self.EXPORT_LOCATION:
			super().openExportPath(**keywords)


	def getHeaderLine(self, **keywords):
		return ('Auftrag', 'Position', 'Pos.Bezeichnung',
				'Möbelname','Bauteil-Definition', 'Bauteil-Bezeichnung', 'Trägermaterial', 'Anzahl',
				'Laenge', 'Breite', 'Dicke','ZLaenge','ZBreite',
				'Drehbar', #0=no 1= Yes
				'Kante_V', 'Dicke_V', 'Kante_H', 'Dicke_H', 'Kante_L', 'Dicke_L', 'Kante_R', 'Dicke_R',
				'Kantencode', 'KantenVN', 'Polygonale Kante','Existierende Kanten',
				'Belag_I', 'Belag_A', 'Beschichtung', 'Bemerkung', 'Zumass',
				'NC_Haupt', 'NC_Neben', 'NC_Format',
				'Name', 'Projektname','Dateiname',
				'Lieferant'
				)

	def GruppeGenerieren(self, Moebelname, MoePosNr, MoePosBeschreibung, Bauteilname):
		Gruppe = MoePosNr + MoePosBeschreibung
		if Gruppe == None or Gruppe == '':
			Gruppe = Moebelname
		if Gruppe == None or Gruppe == '':
			Gruppe = Bauteilname
		self.counter += 1
		return Gruppe




	def TranslateStragety(self, s):
		StratDict={'nc-compatible':'Gemäss NC-Ausgabe','sawn-size':'Rohmass','sawn-size-with-addition':'Rohmass + Zugabe','finished-size':'Fertigmass','finished-size-with-addition':'Fertigmass+Zugabe'}
		return StratDict.get(s)



	def KantenCode(self, Ka1, Ka2, Ka3, Ka4, Ka1S, Ka2S, Ka3S, Ka4S, Ka1E, Ka2E, Ka3E, Ka4E):
		# 1 bündig, 2 vorstehend, 3 Gehrung
		KanteVN = ''
		Kantencode = ''  # Aufpassen! Kantenreihenfolge ist 4 3 1 2 beim Export



		if Ka4 != '':
			if Ka1 == '':
				Kantencode = Kantencode + '1;'
			else:
				Kantencode = Kantencode + self.KaDict[Ka4E][0]+';'

			KanteVN = KanteVN + self.KanteVN

		else:
			Kantencode = Kantencode + '0;'
			KanteVN = KanteVN + '-'

		if Ka3 != '':
			if Ka2 == '':
				Kantencode = Kantencode + '1;'
			else:
				Kantencode = Kantencode + self.KaDict[Ka3E][0]+';'

			KanteVN = KanteVN + self.KanteVN

		else:
			Kantencode = Kantencode + '0;'
			KanteVN = KanteVN + '-'

		if Ka1 != '':
			if Ka3 == '':
				Kantencode = Kantencode + '1;'
			else:
				Kantencode = Kantencode + self.KaDict[Ka1E][0]+';'

			KanteVN = KanteVN + self.KanteVN

		else:
			Kantencode = Kantencode + '0;'
			KanteVN = KanteVN + '-'

		if Ka2 != '':
			if Ka4 == '':
				Kantencode = Kantencode + '1'
			else:
				Kantencode = Kantencode + self.KaDict[Ka2E][0]

			KanteVN = KanteVN + self.KanteVN

		else:
			Kantencode = Kantencode + '0'
			KanteVN = KanteVN + '-'

		return KanteVN, Kantencode


	def getElements(self, **keywords):
		self.export_process = '_'
		entry = keywords['entry']
		projectInfo = keywords['projectInfo']
		positionInfo = keywords['positionInfo']

		finishedSizeLengthGrainDirection, finishedSizeWidthGrainDirection = self.getFinishedSizeGrainDirection(**keywords)

		#Moebelname = self.asString(data=positionInfo['CabinetName'], **keywords)
		PosBeschreibung = self.asString(data=positionInfo["PositionDescription"], **keywords)
		#Bauteilname = self.asString(key='unit_unitid', **keywords)


		Zumass = ''  # 0 kein Zumass / 1 Zumass / leer wie in WL vor dem Import eingestellt




		if self.SpalteV == 'Auto':
			notFormat = re.split('\s+', self.SpalteVKey)
			notFormat.append('dummy')
			Zumass = '1'

			for s in notFormat:
				if s in self.asString(key='part_description', **keywords).lower():
					Zumass = '0'
					break


		else:
			if self.SpalteV == 'leer':
				Zumass = ''
			else:
				Zumass = self.asString(key='part_comment', **keywords)


		PosNr = self.asString(data=positionInfo["PositionNumber"], **keywords)
		Pos = PosNr
		BauteilBez = self.asString(key='part_description', **keywords)
		BauteilName = self.asString(data=entry.name, **keywords)
		TraegerMaterial = self.asString(data=entry.itemID, **keywords)
		
	
		
		Moebelname = self.asString(data=positionInfo['CabinetName'], **keywords)

		
		Gruppe = Moebelname

		
		if (self.asCoordinate(data=entry.boardWidthGrainDirection, **keywords) == '0') and (self.asCoordinate(data=entry.boardThickness, **keywords) == '0'): 
			BauteilBez = 'Beschlag'
			if BauteilName == '':
				BauteilName = TraegerMaterial
				TraegerMaterial = ''
			if Gruppe == '':
				Gruppe = 'Bauteile'
			
		
		if 'Belag' in BauteilName:
			BauteilBez = 'Belag'

		if Moebelname == BauteilBez:
			BauteilBez = ''


		#Swiss Soft Spaltenreihenfolge Kanten 4312

		KanteVN = ''
		Kantencode = ''  # Aufpassen! Kantenreihenfolge ist 4 3 1 2 beim Export

		Ka1 = self.asString(data=entry.edgesGrainDirection.front.supplierID, **keywords)
		Ka2 = self.asString(data=entry.edgesGrainDirection.back.supplierID, **keywords)
		Ka3 = self.asString(data=entry.edgesGrainDirection.left.supplierID, **keywords)
		Ka4 = self.asString(data=entry.edgesGrainDirection.right.supplierID, **keywords)

		Ka1D = self.asCoordinate(data=entry.edgesGrainDirection.front.thickness, **keywords)
		Ka2D = self.asCoordinate(data=entry.edgesGrainDirection.back.thickness, **keywords)
		Ka3D = self.asCoordinate(data=entry.edgesGrainDirection.left.thickness, **keywords)
		Ka4D = self.asCoordinate(data=entry.edgesGrainDirection.right.thickness, **keywords)

		Ka1S = 'Continuous'
		Ka2S = 'Continuous'
		Ka3S = 'Discontinuous'
		Ka4S = 'Discontinuous'

		self.getJointAsString(self.getDataAsString(data=entry.edgesGrainDirection.front.jointEnd, **keywords)),


		Ka1E = self.getJointAsString(self.getDataAsString(data=entry.edgesGrainDirection.front.jointEnd, **keywords))
		Ka2E = self.getJointAsString(self.getDataAsString(data=entry.edgesGrainDirection.back.jointEnd, **keywords))
		Ka3E = self.getJointAsString(self.getDataAsString(data=entry.edgesGrainDirection.left.jointEnd, **keywords))
		Ka4E = self.getJointAsString(self.getDataAsString(data=entry.edgesGrainDirection.right.jointEnd, **keywords))

		Ka1S = self.getJointAsString(self.getDataAsString(data=entry.edgesGrainDirection.front.jointStart, **keywords))
		Ka2S = self.getJointAsString(self.getDataAsString(data=entry.edgesGrainDirection.back.jointStart, **keywords))
		Ka3S = self.getJointAsString(self.getDataAsString(data=entry.edgesGrainDirection.left.jointStart, **keywords))
		Ka4S = self.getJointAsString(self.getDataAsString(data=entry.edgesGrainDirection.right.jointStart, **keywords))


		#self.asString(data=entry.edgesGrainDirection.right.endJoint, **keywords)


		#self.asString(key='part_edge4endjoint', **keywords) #Kantenstoss

		KantenVN, Kantencode = self.KantenCode( Ka1, Ka2, Ka3, Ka4, Ka1S, Ka2S, Ka3S, Ka4S, Ka1E, Ka2E, Ka3E, Ka4E)
		
		debug = str([Ka1+'_'+Ka1D, Ka2+'_'+Ka2D, Ka3+'_'+Ka3D, Ka4+'_'+Ka4D, Ka1S, Ka1E, Ka2S, Ka2E, Ka3S, Ka3E, Ka4S, Ka4E])
		#logging.error('debug')
		

		return (
			self.asString(data=projectInfo["ProjectID"], **keywords),  #Auftrag nicht verwendet beim Import
			Pos, #MUSS Zahl sein und MUSS sortiert sein. Also nicht pos 1, dann 2 dann wieder 1
			PosBeschreibung, #wird nicht verwendet
			Gruppe, #Bauteilbezeichnung oder Bauteilname
			BauteilBez,
			BauteilName,
			TraegerMaterial,
			self.asInt(data=entry.count, **keywords),
			self.asCoordinate(data=finishedSizeLengthGrainDirection, **keywords),
			self.asCoordinate(data=finishedSizeWidthGrainDirection, **keywords),
			self.asCoordinate(data=entry.boardThickness, **keywords), #geändert 1.12.2020 vorher self.asCoordinate(data=entry.thickness, **keywords),
			self.asCoordinate(data=entry.boardLengthGrainDirection, **keywords),
			self.asCoordinate(data=entry.boardWidthGrainDirection, **keywords),
			self.asString(data=self.getSimplifiedGrain(entry.grain)),
			self.asString(data=entry.edgesGrainDirection.front.supplierID, **keywords),
			Ka4D,
			self.asString(data=entry.edgesGrainDirection.back.supplierID, **keywords),
			Ka3D,
			self.asString(data=entry.edgesGrainDirection.left.supplierID, **keywords),
			Ka1D,
			self.asString(data=entry.edgesGrainDirection.right.supplierID, **keywords),
			Ka2D,
			Kantencode,
			KantenVN,
			self.asString(data=entry.polygonalEdgesID, **keywords),
			self.asString(data=entry.polygonalEdges, **keywords),
			self.asString(key='part_covering1', **keywords),
			self.asString(key='part_covering2', **keywords),
			self.asString(key='part_laminate3', **keywords),
			self.asString(key='part_comment', **keywords),
			Zumass,
			self.getNCProgramDisplayName(definition=self.NC_PROGRAM_DISPLAY_NAME_DEFINITION, data=entry.ncProgramPathMain, **keywords),
			self.getNCProgramDisplayName(definition=self.NC_PROGRAM_DISPLAY_NAME_DEFINITION, data=entry.ncProgramPathOpposite, **keywords),
			self.getNCProgramDisplayName(definition=self.NC_PROGRAM_DISPLAY_NAME_DEFINITION, data=entry.ncProgramPathSizing, **keywords),
			self.asString(data=projectInfo['LastName'], **keywords),
			self.asString(data=projectInfo['ProjectCommission'], **keywords),
			self.asString(data=projectInfo['DocumentName'], **keywords),
			
			
			
			#debug,

		)



	def getLocalizedHeader(self, index, default):
		return getLocalizedString(CUTTINGLIST_RESOURCEGROUP, index, default)

	def doClipboardExport(self, **keywords):
		# Excel needs tabulators when pasting from clipboard (LibreOfficeCalc does not care, as it always shows the import dialog)
		self.csvdelimiter = '\t'
		super().doClipboardExport(**keywords)


export = ZuschnittK()
export.doCuttinglistExport()
export.openExportPath()
