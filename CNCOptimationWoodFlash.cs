﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows;
using System.Text.RegularExpressions;
using System.Linq;

namespace Vectorworks_Optimizer
{
    class CNCOptimationWoodFlash
    {
        bool testfehler = false;
        int testfehlerindex = 2;
        private string formating = "";
        private string ncMain = "";
        private string ncMain2 = "";
        private string ncNext = "";
        private string ncNext2 = "";
        private string oldNCPath = "";
        private string warningRightThroughEditStart = "W#4{ ::WTg WS=24  #8060=0 #8065=0 #8061=0 #8066=0 #8062=0 #8850=Durchgehend PosX";
        private string warningRightThroughEditEnd = " }W";
        private string warningXPostitionHolderMain = "";
        private string warningXPostitionHolderNext = "";
        private string squareFunctionName = "#8098=squad.tmcr";
        private string contourFunctionName = "W#89{";
        private string contourLineName = "W#2201{";
        private string contourCurveName = "W#2111{";
        private string miterFunctionName = "W#1052{";
        private string miterAngleName = "#8519=";
        private string mirrorEditVertical = "2*(l/2)-";
        private string mirrorEditHorizontal = "2*(hf/2)-";
        private string fictionSideGroundWord = "GSIDE#";
        private string offsetStart = "OFFS{";
        private string offsetEnd = "}OFFS";
        private string offsetDistanz = "3";
        private string drilling = "W#81{";
        private string drillingDiameter = "#1002=";
        private string drillingX = "#1=";
        private string drillingY = "#2=";
        private string drillingMacro = "#8098=cerchio.tmcr";
        private string drillingMacroToolNr = "#8153=";
        private string drillingMacroRadius = "#8509";
        private string drillingMacroX = "#8020=";
        private string drillingMacroY = "#8021=";
        private string drillingMacroZ = "#8145=-";

        private bool squareFormating = false;
        private bool editFrontSideMain = false;
        private bool editBackSideMain = false;
        private bool editLeftSideMain = false;
        private bool editRightSideMain = false;
        private bool editFictionSideMain = false;
        private bool editFrontSideNext = false;
        private bool editBackSideNext = false;
        private bool editLeftSideNext = false;
        private bool editRightSideNext = false;
        private bool editFictionSideNext = false;
        private bool edgeFrontSideMain = false;
        private bool edgeBackSideMain = false;
        private bool edgeLeftSideMain = false;
        private bool edgeRightSideMain = false;
        private bool edgeFictionSideMain = false;
        private bool edgeFrontSideNext = false;
        private bool edgeBackSideNext = false;
        private bool edgeLeftSideNext = false;
        private bool edgeRightSideNext = false;
        private bool edgeFictionSideNext = false;

        private bool miterFrontSideMain = false;
        private bool miterBackSideMain = false;
        private bool miterLeftSideMain = false;
        private bool miterRightSideMain = false;
        private bool miterFictionSideMain = false;
        private bool miterFrontSideNext = false;
        private bool miterBackSideNext = false;
        private bool miterLeftSideNext = false;
        private bool miterRightSideNext = false;
        private bool miterFictionSideNext = false;

        private bool contourFrontSideMain = false;
        private bool contourBackSideMain = false;
        private bool contourLeftSideMain = false;
        private bool contourRightSideMain = false;
        private bool contourFictionSideMain = false;
        private bool contourFrontSideNext = false;
        private bool contourBackSideNext = false;
        private bool contourLeftSideNext = false;
        private bool contourRightSideNext = false;
        private bool contourFictionSideNext = false;

        private bool drillingFrontSideMain = false;
        private bool drillingBackSideMain = false;
        private bool drillingLeftSideMain = false;
        private bool drillingRightSideMain = false;
        private bool drillingFictionSideMain = false;
        private bool drillingFrontSideNext = false;
        private bool drillingBackSideNext = false;
        private bool drillingLeftSideNext = false;
        private bool drillingRightSideNext = false;
        private bool drillingFictionSideNext = false;

        private bool drillingMacroFrontSideMain = false;
        private bool drillingMacroBackSideMain = false;
        private bool drillingMacroLeftSideMain = false;
        private bool drillingMacroRightSideMain = false;
        private bool drillingMacroFictionSideMain = false;
        private bool drillingMacroFrontSideNext = false;
        private bool drillingMacroBackSideNext = false;
        private bool drillingMacroLeftSideNext = false;
        private bool drillingMacroRightSideNext = false;
        private bool drillingMacroFictionSideNext = false;

        private bool maxLength = false;
        private bool maxWidth = false;

        private bool isWarningRightThroughEdit = false;

        private float marginDistance = OptionOptimizerCNCToolsSetting.Default.tb_minEdgeDistance;
        private float minDrilling = OptionOptimizerCNCToolsSetting.Default.tb_minEdgeDrilling;
        private float minDrillingDistance = 2;
        private float maxLengthMaschine = 3800f;
        private float maxWidthMaschine = 1500f;
        private float maxDepthTool = 0;
        private float componentLength = 0;
        private float componentWidth = 0;
        private float componentThick = 0;
        private List<string> ncFormatingList = new List<string>();
        private List<string> ncMainList = new List<string>();
        private List<string> ncNextList = new List<string>();
        private List<string> controllList = new List<string>();       
        private int firstNumberOfLettersToTakeOver = 5;
        private int lastNumberOfLettersToTakeOver = 2;

        public void cncOptimation (string formatingOld, string ncMainOld, string ncNextOld, 
                                    string oldPath, string positionNr, string furniturName, string newPath, string componentNr, string componentDefinition,
                                    float thick,
                                    string edgeFront, string edgeBack, string edgeLeft, string edgeRight, string polygonalEdge, string existEdge,
                                    string editType, bool edgeBevorSurface, List<string> takeControllList)
        {           
            
            controllList = takeControllList;
            clearLists();
            saveOldNCPath(oldPath, positionNr, furniturName);
            squareFormating = false;
            //Edit recognize
            readNC(formatingOld, ncMainOld, ncNextOld);
            if(edgeFront=="" &&
                edgeBack==""&&
                edgeRight==""&&
                edgeLeft=="") if (existEdge != "") { edgeFictionSideMain = true; edgeFictionSideNext = true; }
                        
            if (ncMainOld == "" && ncNextOld == "")
            {
                optimationForming(true, editType == OptionOptimizerCNCToolsSetting.Default.matDefDia, componentThick);                
                ncMain = createNewFileName(formatingOld, newPath, componentNr, "H");
                createNCCode(ncMain, ncFormatingList);
                fillControllerList(ncMain, componentThick);
            }
            else if (ncMainOld == "" && ncNextOld != "")
            {
                optimationForming(false, editType == OptionOptimizerCNCToolsSetting.Default.matDefDia, componentThick);
                ncNextList = sortMacro(ncNextList);
                if ((edgeFront != "" && (editFrontSideNext || contourFrontSideNext || miterRightSideNext || miterLeftSideNext)) ||
                    (edgeBack != "" && (editBackSideNext || contourBackSideNext || miterRightSideNext || miterLeftSideNext)) ||
                    (edgeLeft != "" && (editRightSideNext || contourRightSideNext || miterFrontSideNext || miterBackSideNext)) ||
                    (edgeRight != "" && (editLeftSideNext || contourLeftSideNext || miterFrontSideNext || miterBackSideNext)) ||
                    (edgeFictionSideMain && ((editFrontSideNext || editBackSideNext || editRightSideNext || editLeftSideNext) ||
                    (contourFrontSideNext || contourBackSideNext || contourRightSideNext || contourLeftSideNext || editBackSideNext) ||
                    (miterRightSideNext || miterLeftSideNext || miterFrontSideNext || miterBackSideNext))))
                {                    
                    ncNext = createNewFileName(formatingOld, newPath, componentNr, "F");
                    ncNext2 = createNewFileName(ncNextOld, newPath, componentNr, "N");
                    if (isWarningRightThroughEdit) ncNextList = insertWarningRightThourgh(ncNextList,warningXPostitionHolderNext);
                    if (OptionOptimizerCNCToolsSetting.Default.cb_doContourOptimiazation) ncNextList = startEditsOnEdgeSite(ncNextList, false);
                    createNCCode(ncNext, ncFormatingList);
                    createNCCode(ncNext2, ncNextList);
                }
                else
                {
                    ncNextList = sortMacro(ncNextList);
                    ncNext = createNewFileName(ncNextOld, newPath, componentNr, "N");
                    ncNextList = combineFormingAndNC(ncFormatingList, ncNextList);
                    if (isWarningRightThroughEdit) ncNextList = insertWarningRightThourgh(ncNextList, warningXPostitionHolderNext);
                   
                    createNCCode(ncNext, ncNextList);
                }
                fillControllerList(ncNext, thick);
            }
            else if (ncMainOld != "")
            {
                optimationForming(true, editType == OptionOptimizerCNCToolsSetting.Default.matDefDia, componentThick);
                ncMainList = sortMacro(ncMainList);
                if ((edgeFront != "" && (editFrontSideMain || contourFrontSideMain || miterRightSideMain || miterLeftSideMain)) ||
                    (edgeBack != "" && (editBackSideMain || contourBackSideMain || miterRightSideMain || miterLeftSideMain)) ||
                    (edgeRight != "" && (editRightSideMain || contourRightSideMain || miterFrontSideMain || miterBackSideMain)) ||
                    (edgeLeft != "" && (editLeftSideMain || contourLeftSideMain || miterFrontSideMain || miterBackSideMain)) ||
                    (edgeFictionSideMain && ((editFrontSideMain || editBackSideMain || editRightSideMain || editLeftSideMain) ||
                    (contourFrontSideMain || contourBackSideMain || contourRightSideMain || contourLeftSideMain || editBackSideMain) ||
                    (miterRightSideMain || miterLeftSideMain || miterFrontSideMain || miterBackSideMain))))
                {                   
                    ncMain = createNewFileName(formatingOld, newPath, componentNr, "F");
                    ncMain2 = createNewFileName(ncMainOld, newPath, componentNr, "H");
                    if (isWarningRightThroughEdit) ncMainList = insertWarningRightThourgh(ncMainList, warningXPostitionHolderMain);
                    if (OptionOptimizerCNCToolsSetting.Default.cb_doContourOptimiazation) ncMainList = startEditsOnEdgeSite(ncMainList, true);
                    createNCCode(ncMain, ncFormatingList);
                    createNCCode(ncMain2, ncMainList);
                    
                }
                else
                {   ncMainList = sortMacro(ncMainList);                 
                    ncMain = createNewFileName(ncMainOld, newPath, componentNr, "H");
                    ncMainList = combineFormingAndNC(ncFormatingList, ncMainList);
                    if (isWarningRightThroughEdit) ncMainList = insertWarningRightThourgh(ncMainList, warningXPostitionHolderMain);
                    
                    createNCCode(ncMain, ncMainList);
                }
                if (ncNextOld != "")
                {
                    ncNextList = sortMacro(ncNextList);
                    ncNext = createNewFileName(ncNextOld, newPath, componentNr, "N");
                    if (isWarningRightThroughEdit) ncNextList = insertWarningRightThourgh(ncNextList, warningXPostitionHolderNext);
                    if (OptionOptimizerCNCToolsSetting.Default.cb_doContourOptimiazation) ncNextList = startEditsOnEdgeSite(ncNextList, false);
                    createNCCode(ncNext, ncNextList);
                }
                fillControllerList(ncMain, componentThick);
            }                     
        }
        private void saveOldNCPath(string oldPath, string positionNr, string furniturName)
        {
            string positionNrBuild = "";
            string furniturNameBuild = "";
           
            if (positionNr != "") positionNrBuild = @"\" + positionNr;
            if (furniturName != "") furniturNameBuild = @"\" + furniturName;

            oldNCPath = oldPath + positionNrBuild + furniturNameBuild + @"\";
        }
        private void clearLists()
        {
            ncFormatingList.Clear();
            ncMainList.Clear();
            ncNextList.Clear();

            squareFormating = false;
            editFrontSideMain = false;
            editBackSideMain = false;
            editLeftSideMain = false;
            editRightSideMain = false;
            editFictionSideMain = false;
            editFrontSideNext = false;
            editBackSideNext = false;
            editLeftSideNext = false;
            editRightSideNext = false;
            editFictionSideNext = false;
            edgeFrontSideMain = false;
            edgeBackSideMain = false;
            edgeLeftSideMain = false;
            edgeRightSideMain = false;
            edgeFictionSideMain = false;
            edgeFrontSideNext = false;
            edgeBackSideNext = false;
            edgeLeftSideNext = false;
            edgeRightSideNext = false;
            edgeFictionSideNext = false;

            miterFrontSideMain = false;
            miterBackSideMain = false;
            miterLeftSideMain = false;
            miterRightSideMain = false;
            miterFictionSideMain = false;
            miterFrontSideNext = false;
            miterBackSideNext = false;
            miterLeftSideNext = false;
            miterRightSideNext = false;
            miterFictionSideNext = false;

            contourFrontSideMain = false;
            contourBackSideMain = false;
            contourLeftSideMain = false;
            contourRightSideMain = false;
            contourFictionSideMain = false;
            contourFrontSideNext = false;
            contourBackSideNext = false;
            contourLeftSideNext = false;
            contourRightSideNext = false;
            contourFictionSideNext = false;
            float maxDepthTool = 0;
            formating = "";
            ncMain = "";
            ncMain2 = "";
            ncNext = "";
            ncNext2 = "";
            oldNCPath = "";


        }
        private void readNC(string formatingOld, string ncMainOld, string ncNextOld)
        {
            if (formatingOld != "") readNCFormating(formatingOld);
            if (ncMainOld != "") readNCMain(ncMainOld);
            if (ncNextOld != "") readNCNext(ncNextOld);
        }
        private void readNCFormating(string fileName)
        {
            //read cn formating
            using(StreamReader sr = new StreamReader(oldNCPath + fileName, Encoding.GetEncoding(1252)))
            {
                string line;
                bool offsetChange = false;
                while (( line = sr.ReadLine())!= null){
                    if (line.Contains(offsetEnd)) offsetChange = false;
                    if (line.Contains(offsetStart)) offsetChange = true;
                    if (offsetChange && line.Contains("#0=")) line = "#0=" + offsetDistanz + "|" + offsetDistanz;
                    if (offsetChange && line.Contains("#1=")) line = "#1=" + offsetDistanz + "|" + offsetDistanz;
                    ncFormatingList.Add(line);
                    if (line.Contains(squareFunctionName)) squareFormating = true;
                    if (line.Contains("::UNm"))
                    {
                        string[] temp = line.Split(" ");
                        foreach (string currentLine in temp)
                        {
                            if (currentLine.Contains("DL=")) componentLength = getOnlyFloat(currentLine);
                            if (currentLine.Contains("DH=")) componentWidth = getOnlyFloat(currentLine);
                            if (currentLine.Contains("DS=")) componentThick = getOnlyFloat(currentLine);
                        }
                    }
                    
                }
            }
            
        }
        private void readNCMain(string fileName)
        {
           
            using (StreamReader sr = new StreamReader(oldNCPath + fileName, Encoding.GetEncoding(1252)))
            {
                bool controllSide1Edits = false;
                float xPosition = 0;
                float yPosition = 0;
                
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    ncMainList.Add(line);
                    
                    if (line.Contains("SIDE#3")) editFrontSideMain = true;
                    if (line.Contains("SIDE#4")) editRightSideMain = true;
                    if (line.Contains("SIDE#5")) editBackSideMain = true;
                    if (line.Contains("SIDE#6")) editLeftSideMain = true;
                    if (line.Contains(fictionSideGroundWord)) editFictionSideMain = true;
                    if (line.Contains("$=Kante Vorne:")) edgeFrontSideMain = true;
                    if (line.Contains("$=Kante Rechts:")) edgeRightSideMain = true;
                    if (line.Contains("$=Kante Hinten:")) edgeBackSideMain = true;
                    if (line.Contains("$=Kante Links:")) edgeLeftSideMain = true;
                    if (line.Contains(miterFunctionName))
                    {
                        float angle = 0;
                        string[] temp = line.Split(" ");
                        foreach (string currentLine in temp)
                        {
                            if (currentLine.Contains(miterAngleName))
                            {
                                angle = Convert.ToSingle(currentLine.Replace(miterAngleName, ""));
                                break;
                            }
                        }
                        if (angle < 90 && angle > -90) miterBackSideMain = true;
                        if (angle < 180 && angle > -0) miterLeftSideMain = true;
                        if (angle < -90) miterFrontSideMain = true;
                        if (angle > 90) miterFrontSideMain = true;
                        if (angle < -0 && angle > -180) miterRightSideMain = true;
                    }
                    if (line.Contains("SIDE#1{")) { controllSide1Edits = true; continue; }
                    if (controllSide1Edits)
                    {
                        if (line.Contains("}SIDE")) controllSide1Edits = false;
                        if (line.Contains(contourFunctionName) || line.Contains(contourLineName) || line.Contains(contourCurveName) || line.Contains(drillingMacro) ||line.Contains(drilling))
                        {
                            xPosition = 0;
                            yPosition = 0;
                            float radius = 0;
                            float margeCurrent = marginDistance;

                            string[] temp = line.Split(" ");
                            foreach (string currentLine in temp)
                            {
                                if (currentLine.Contains(" #1=") || currentLine.Contains(drillingX)|| currentLine.Contains(drillingMacroX)) xPosition = getOnlyFloat(currentLine);
                                if (currentLine.Contains(" #2=") || currentLine.Contains(drillingY) || currentLine.Contains(drillingMacroY)) yPosition = getOnlyFloat(currentLine);
                                if (currentLine.Contains(drillingMacroRadius))
                                {
                                    radius = getOnlyFloat(currentLine);
                                    if (radius > minDrilling / 2) margeCurrent = minDrillingDistance + radius;
                                    else margeCurrent = marginDistance - radius;
                                }
                                if (currentLine.Contains(drillingDiameter))
                                {                                    
                                    radius = getOnlyFloat(currentLine) / 2;
                                    if (radius > minDrilling / 2) margeCurrent = 2 + radius;
                                    else margeCurrent = minDrillingDistance - radius;
                                }
                               
                                if (currentLine.Contains(" #3=") || currentLine.Contains(drillingMacroZ))
                                {                                    
                                    float zPosition = getOnlyFloat(currentLine.Replace("-", ""));
                                    if (zPosition >= componentThick)
                                    {                                        
                                        if (xPosition > 20 && yPosition > 20 && xPosition < componentLength - 20 && yPosition > componentWidth - 20)
                                        {
                                            isWarningRightThroughEdit = true;
                                            if (!warningXPostitionHolderNext.Contains(xPosition + " |")) warningXPostitionHolderNext += xPosition + "|";
                                        }
                                    }                                    
                                    break;
                                }
                            }
                            if (OptionOptimizerCNCToolsSetting.Default.cb_doMinEdgeDistance == true)
                            {
                                if (xPosition < margeCurrent) contourLeftSideMain = true;
                                if (xPosition > componentLength - margeCurrent) contourRightSideMain = true;
                                if (yPosition < margeCurrent) contourFrontSideMain = true;
                                if (yPosition > componentWidth - margeCurrent) contourBackSideMain = true;
                            }
                        }
                    }

                }
                if (componentLength >= maxLengthMaschine) maxLength = true;
                if (componentWidth >= maxWidthMaschine) maxWidth = true;
            }
        }
        private void readNCNext(string fileName)
        {
            using (StreamReader sr = new StreamReader(oldNCPath + fileName, Encoding.GetEncoding(1252)))
            {
                bool controllSide1Edits = false;
                float xPosition = 0;
                float yPosition = 0;
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    ncNextList.Add(line);
                    
                    if (line.Contains("SIDE#3")) editFrontSideNext = true;
                    if (line.Contains("SIDE#4")) editRightSideNext = true;
                    if (line.Contains("SIDE#5")) editBackSideNext = true;
                    if (line.Contains("SIDE#6")) editLeftSideNext = true;
                    if (line.Contains(fictionSideGroundWord)) editFictionSideNext = true;
                    if (line.Contains("$=Kante Vorne:")) edgeFrontSideNext = true;
                    if (line.Contains("$=Kante Rechts:")) edgeRightSideNext = true;
                    if (line.Contains("$=Kante Hinten:")) edgeBackSideNext = true;
                    if (line.Contains("$=Kante Links:")) edgeLeftSideNext = true;
                    if (line.Contains(miterFunctionName))
                    {
                        float angle = 0;
                        string[] temp = line.Split(" ");
                        foreach (string currentLine in temp)
                        {
                            if (currentLine.Contains(miterAngleName))
                            {
                                angle = Convert.ToSingle(currentLine.Replace(miterAngleName,""));
                                break;
                            }
                        }
                        if (angle < 90 && angle > -90) miterBackSideNext = true;
                        if (angle < 180 && angle > -0) miterLeftSideNext = true;
                        if (angle < -90) miterFrontSideNext = true;
                        if (angle > 90) miterFrontSideNext = true;
                        if (angle < -0 && angle > -180) miterRightSideNext = true;
                    }
                    if (line.Contains("SIDE#1{")) { controllSide1Edits = true; continue; }
                    if (controllSide1Edits)
                    {
                        if (line.Contains("}SIDE")) controllSide1Edits = false;
                        if (line.Contains(contourFunctionName) || line.Contains(contourLineName) || line.Contains(contourCurveName) || line.Contains(drillingMacro) || line.Contains(drilling))
                        {
                            xPosition = 0;
                            yPosition = 0;
                            float radius = 0;
                            float margeCurrent = marginDistance;

                            string[] temp = line.Split(" ");
                            foreach (string currentLine in temp)
                            {
                                if (currentLine.Contains("#1=") || currentLine.Contains(drillingX) || currentLine.Contains(drillingMacroX)) xPosition = getOnlyFloat(currentLine);
                                if (currentLine.Contains("#2=") || currentLine.Contains(drillingY) || currentLine.Contains(drillingMacroY)) yPosition = getOnlyFloat(currentLine);
                                if (currentLine.Contains(drillingMacroRadius))
                                {
                                    radius = getOnlyFloat(currentLine);
                                    if (radius > minDrilling / 2) margeCurrent = minDrillingDistance + radius;
                                    else margeCurrent = marginDistance - radius;
                                }
                                if (currentLine.Contains(drillingDiameter))
                                {
                                    radius = getOnlyFloat(currentLine) / 2;
                                    if (radius > minDrilling / 2) margeCurrent = 2 + radius;
                                    else margeCurrent = minDrillingDistance - radius;
                                }
                                if (currentLine.Contains("#3=") || currentLine.Contains(drillingMacroZ))
                                {
                                    float zPosition = getOnlyFloat(currentLine.Replace("-", ""));
                                    if (zPosition >= componentThick)
                                    {
                                        if (xPosition > 20 && yPosition > 20 && xPosition < componentLength - 20 && yPosition > componentWidth - 20)
                                        {
                                            isWarningRightThroughEdit = true;
                                            if (!warningXPostitionHolderNext.Contains(xPosition + " |")) warningXPostitionHolderNext += xPosition + "|";
                                        }
                                    }
                                    break;                                   
                                }
                            }
                            if (OptionOptimizerCNCToolsSetting.Default.cb_doMinEdgeDistance == true)
                            {
                                if (xPosition < marginDistance - radius) contourLeftSideNext = true;
                                if (xPosition > componentLength - marginDistance + radius) contourRightSideNext = true;
                                if (yPosition < marginDistance - radius) contourFrontSideNext = true;
                                if (yPosition > componentWidth - marginDistance + radius) contourBackSideNext = true;
                            }
                        }
                    }

                }

                if (componentLength >= maxLengthMaschine) maxLength = true;
                if (componentWidth >= maxWidthMaschine) maxWidth = true;
            }
        }
        private void optimationForming(bool sideH, bool editTypeDia, float thick)
        {
            int startSideChange = 0;
            int endSideChange = 0;
            string toolNr = "";
            string toolDistance = "";
            float toolMaxthick = 0;
            float toolMaxDipping = 0;
            bool toolMiddle = false;
            float toolMinDepth = 0;
            bool doMirror = false;
            string tempNameInZ = "";
            float tempPaceInZ = 0;
            float tempMinDepth = 0;
            int repeat = 0;
            bool isSquare = true;
            if (!sideH) doMirror = true;

            for (int i = 0; i < ncFormatingList.Count; i++)
            {              
                
                if (ncFormatingList[i].Contains(squareFunctionName) )
                {
                    isSquare = true;
                    if (editTypeDia && componentThick<= Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolDiaMaxThick))
                    {
                        toolNr = OptionOptimizerCNCToolsSetting.Default.squareToolDiaNr;
                        toolDistance = OptionOptimizerCNCToolsSetting.Default.squareToolDiaDistance;
                        toolMaxthick = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolDiaMaxThick);
                        toolMaxDipping = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolDiaMaxDipping);
                        toolMiddle = OptionOptimizerCNCToolsSetting.Default.squareToolDiaMiddle;
                        toolMinDepth = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolDiaMinDepth);
                    }
                    else if (componentThick <= Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolHSMaxThick))
                    {
                        toolNr = OptionOptimizerCNCToolsSetting.Default.squareToolHSNr;
                        toolDistance = OptionOptimizerCNCToolsSetting.Default.squareToolHSDistance;
                        toolMaxthick = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolHSMaxThick);
                        toolMaxDipping = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolHSMaxDipping);
                        toolMiddle = OptionOptimizerCNCToolsSetting.Default.squareToolHSMiddle;
                        toolMinDepth = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolHSMinDepth);
                    }
                    else
                    {
                        toolNr = OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyNr;
                        toolDistance = OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyDistance;
                        toolMaxthick = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyMaxThick);
                        toolMaxDipping = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyMaxDipping);
                        toolMiddle = OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyMiddle;
                        toolMinDepth = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolEmergencyMinDepth);
                    }
                }
                else if (ncFormatingList[i].Contains(contourFunctionName))
                {
                    isSquare = false;
                    if (editTypeDia && componentThick <= Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMaxThick))
                    {
                        toolNr = OptionOptimizerCNCToolsSetting.Default.shapeToolDiaNr;
                        toolDistance = OptionOptimizerCNCToolsSetting.Default.shapeToolDiaDistance;
                        toolMaxthick = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMaxThick);
                        toolMaxDipping = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMaxDipping);
                        toolMiddle = OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMiddle;
                        toolMinDepth = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.shapeToolDiaMinDepth);
                    }
                    else if (componentThick <= Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.shapeToolHSMaxThick))
                    {
                        toolNr = OptionOptimizerCNCToolsSetting.Default.shapeToolHSNr;
                        toolDistance = OptionOptimizerCNCToolsSetting.Default.shapeToolHSDistance;
                        toolMaxthick = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.shapeToolHSMaxThick);
                        toolMaxDipping = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.shapeToolHSMaxDipping);
                        toolMiddle = OptionOptimizerCNCToolsSetting.Default.shapeToolHSMiddle;
                        toolMinDepth = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.shapeToolHSMinDepth);
                    }
                    else
                    {
                        toolNr = OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyNr;
                        toolDistance = OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyDistance;
                        toolMaxthick = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyMaxThick);
                        toolMaxDipping = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyMaxDipping);
                        toolMiddle = OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyMiddle;
                        toolMinDepth = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.shapeToolEmergencyMinDepth);
                    }

                }
                
                repeat = (int)Math.Floor(componentThick / (toolMaxDipping - toolMinDepth * 2));
                //is rectangle
                if (ncFormatingList[i].Contains(squareFunctionName))
                {
                    startSideChange = i;
                    endSideChange = i;

                    string[] temp = ncFormatingList[i].Split(" ");
                    foreach (string currentLine in temp)
                    {
                        if (currentLine.Contains("#8500=")) ncFormatingList[i] = ncFormatingList[i].Replace(currentLine, "#8500=" +toolDistance);
                        if (currentLine.Contains("#8501=")) ncFormatingList[i] = ncFormatingList[i].Replace(currentLine, "#8501=" + toolDistance);
                        if (currentLine.Contains("#8502=")) ncFormatingList[i] = ncFormatingList[i].Replace(currentLine, "#8502=" + toolNr);
                        if (currentLine.Contains("#8503="))
                        {
                            maxDepthTool = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolHSMaxThick);
                            if (toolMiddle)
                            {
                                tempMinDepth = 0;
                                tempPaceInZ = componentThick / (repeat + 2);
                                tempNameInZ = "#8503=-" + tempPaceInZ;                                
                                ncFormatingList[i] = ncFormatingList[i].Replace(currentLine, tempNameInZ);
                            }
                            else
                            {
                                tempMinDepth = toolMinDepth;
                                tempPaceInZ = componentThick / (repeat + 1);
                                tempNameInZ = "#8503=-" + (tempPaceInZ - toolMinDepth);
                                ncFormatingList[i] = ncFormatingList[i].Replace(currentLine, tempNameInZ);
                            }

                        }
                    }
                    break;
                }
                //formating
                else if (ncFormatingList[i].Contains(contourFunctionName))
                {
                    
                    startSideChange = i;
                   

                    string[] temp = ncFormatingList[i].Split(" ");
                    foreach (string currentLine in temp)
                    {
                        if (currentLine.Contains("#8182="))
                        {
                            ncFormatingList[i] = ncFormatingList[i].Replace(currentLine, "#8182=" + toolDistance);
                        }
                        if (currentLine.Contains("#8187="))
                        {
                            ncFormatingList[i] = ncFormatingList[i].Replace(currentLine, "#8187=" + toolDistance);
                        }
                        if (currentLine.Contains("#205="))
                        {
                            ncFormatingList[i] = ncFormatingList[i].Replace(currentLine, "#205=" + toolNr);
                        }
                        if (currentLine.Contains("#3="))
                        {
                            maxDepthTool = Convert.ToSingle(OptionOptimizerCNCToolsSetting.Default.squareToolHSMaxThick);
                            if (toolMiddle)
                            {
                                tempMinDepth = 0;
                                tempPaceInZ = componentThick / (repeat + 2);
                                tempNameInZ = "#3=-" + tempPaceInZ;
                                ncFormatingList[i] = ncFormatingList[i].Replace(currentLine, tempNameInZ);
                            }
                            else
                            {
                                tempMinDepth = toolMinDepth;
                                tempPaceInZ = componentThick / (repeat + 1);
                                tempNameInZ = "#3=-" + (tempPaceInZ - toolMinDepth);
                                ncFormatingList[i] = ncFormatingList[i].Replace(currentLine, tempNameInZ);
                            }

                        }
                        if (doMirror && currentLine.Contains("#1=") && ncFormatingList[i].Contains("#205="))
                        {
                            ncFormatingList[i] = ncFormatingList[i].Replace("#1=", "#1=" + mirrorEditVertical);
                        }
                    }
                    continue;
                }
                else if (ncFormatingList[i].Contains("}SIDE"))
                {
                    endSideChange = i - 1;
                }



            }
            if (!sideH)
            {
                
                //get cordinate from next
                List<String> tempXYCordinate = new List<string>();
               
                for (int j = startSideChange+1; j <= endSideChange ; j++)
                {
                    String[] temp = ncFormatingList[j].Split();
                    String positionXY = "";

                    foreach (String currentPosition in temp)
                    {
                        if(currentPosition.Contains("#1="))
                        {
                            positionXY += currentPosition + " ";
                        }
                        if (currentPosition.Contains("#2="))
                        {
                            positionXY += currentPosition;
                            break;
                        }
                    }
                   
                    tempXYCordinate.Add(positionXY);
                }
                //change position
                for (int j = 1 ; j < tempXYCordinate.Count() ; j++)
                {
                   
                    ncFormatingList[startSideChange + 1 + j] = ncFormatingList[startSideChange + 1 + j].Replace(tempXYCordinate[j], tempXYCordinate[j - 1]).Replace("#1=", "#1=" + mirrorEditVertical);
                }
                //change first
                ncFormatingList[startSideChange + 1] = ncFormatingList[startSideChange + 1].Replace(tempXYCordinate[0], tempXYCordinate[tempXYCordinate.Count() - 1]).Replace("#1=", "#1=" + mirrorEditVertical);
                //add change
                for (int j = startSideChange + 2; j <= endSideChange; j++)
                {
                    String temp = ncFormatingList[j];
                    ncFormatingList.RemoveAt(j);
                    ncFormatingList.Insert(startSideChange + 1, temp);
                }    
                
            }
            if (repeat > 0)
            {
                int elementCount = endSideChange - startSideChange + 1;
                for (int i = 1; i <= repeat; i++)
                {
                    for (int j = startSideChange; j < startSideChange + elementCount; j++)
                    {
                        if (isSquare)
                        {
                            ncFormatingList.Insert(j + i, ncFormatingList[startSideChange].Replace(tempNameInZ, "#8503=-" + (tempPaceInZ * (i + 1) - tempMinDepth)));
                        }
                        else
                        {
                            ncFormatingList.Insert(j + elementCount * i, ncFormatingList[j].Replace(tempNameInZ, "#3=-" + (tempPaceInZ * (i + 1) - tempMinDepth)));
                        }
                    }

                }
                
                
            }
        }
        private List<string> sortMacro(List<string> ncCode)
        {
            string tempSortName = "";
            for(int i = 0; i < ncCode.Count - 1; i++)
            {
                if (ncCode[i].Contains("#8098=99"))
                {                    
                    string[] temp = ncCode[i].Split(" ");
                    i++;
                    foreach(string currentLine in temp)
                    {
                        if (currentLine.Contains("#8098=99")) tempSortName = currentLine;
                    }
                    for(int j = i+1;j< ncCode.Count - 1; j++)
                    {
                        if (ncCode[j].Contains(tempSortName))
                        {
                            ncCode.Insert(i, ncCode[j]);
                            ncCode.RemoveAt(j + 1);
                            i++;
                        }
                    }
                }
            }

            return ncCode;
        }
        private void fillControllerList(string filename, float thick)
        {
            if (thick > maxDepthTool) controllList.Add(filename + '\t' + "Bauteil ist zu dick für den Fräser, kontrollieren");
            if(maxLength) controllList.Add(filename + '\t' + "Bauteil ist zu lang für die Maschine");
            if (maxWidth) controllList.Add(filename + '\t' + "Bauteil ist zu breit für die Maschine");
        }
        private void createNCCode ( string newNameWithPath, List<string> ncList)
        {
            using(StreamWriter sw = new StreamWriter(newNameWithPath))
            {
                foreach(string currentLine in ncList)
                {
                    sw.WriteLine(currentLine);
                }
            }  
           
        }
        private List<string> combineFormingAndNC(List<string> forming, List<string> ncCode)
        {
            int startCopieForming = 0;
            int endCopieForming = 0;
            bool offsetChange = false;
            
            
            for (int i = 10; i < forming.Count; i++)
            {               
                if (forming[i].Contains("SIDE#1{"))
                {
                    // if (forming[i + 1].Contains("$=Obere Fläche")) startCopieForming = i + 2;
                    // else startCopieForming = i + 1;
                    startCopieForming = i;
                }
                if (forming[i].Contains("}SIDE")) { endCopieForming = i ; break; }
            }
            for (int i =10; i < ncCode.Count-1; i++)
            {
                //add Geos
                
                if (ncCode[i].Contains("}LINK"))
                {
                    
                    if (ncCode[i+1].Contains("GEO{ ::"))
                    {
                       
                        for (int j = i; j < ncCode.Count; j++)
                        {
                            if (ncCode[j].Contains("}GEO"))
                            {
                                i = j;
                                break;
                            }
                        }

                    }
                    
                    // if (ncCode[i + 1].Contains("SIDE#1{"))
                    // {
                    //     if (ncCode[i + 2].Contains("$=Obere Fläche")) i += 3;
                    //     else i += 2;
                    // }
                    i++;
                    for (int j = startCopieForming; j <= endCopieForming; j++)
                    {
                        ncCode.Insert(i, forming[j]);
                        i++;
                    }
                }

                if (ncCode[i].Contains(offsetEnd)) offsetChange = false;
                if (ncCode[i].Contains(offsetStart)) offsetChange = true;
                if (offsetChange && ncCode[i].Contains("#0=")) ncCode[i] = "#0="+offsetDistanz+"|"+offsetDistanz;
                if (offsetChange && ncCode[i].Contains("#1=")) ncCode[i] = "#1=" + offsetDistanz + "|" + offsetDistanz;
            }
            return ncCode;
        }
        private List<string> insertWarningRightThourgh(List<string> ncCode, string warningHolder)
        {
            if (warningHolder != "")
            {
                for (int i = 10; i < ncCode.Count; i++)
                {
                    if (ncCode[i].Contains("SIDE#1{"))
                    {
                        if (ncCode[i + 1].Contains("Obere Fläche")) ncCode.Insert(i + 2, warningRightThroughEditStart + warningHolder + warningRightThroughEditEnd);
                        else ncCode.Insert(i+1, warningRightThroughEditStart + warningHolder + warningRightThroughEditEnd);
                    }
                }
            }
            return ncCode;
        }
        private List<string> startEditsOnEdgeSite(List<string> ncCode, bool sideH)
        {
            bool editSide1 = false;
            bool editSide3 = false;
            bool editSide4 = false;
            bool editSide5 = false;
            bool editSide6 = false;
            bool edgefront = false;
            bool edgeBack = false;
            bool edgeLeft = false;
            bool edgeRight = false;
            bool doStartContour = false;
            bool doEndContour = false;
            bool doChange = false;
            bool startOnEdge = false;
            bool endOnEdge = false;
            bool doReset = false;
            int startContourIndex = 0;
            int endContourIndex = 0;
            int countLines = 0;
            float startX = 0;
            float startY = 0;
            float endX = 0;
            float endY = 0;
            if (sideH)
            {
                edgefront = edgeFrontSideMain;
                 edgeBack = edgeBackSideMain;
                 edgeLeft = edgeLeftSideMain;
                 edgeRight = edgeRightSideMain;
            }
            else
            {
                edgefront = edgeFrontSideNext;
                edgeBack = edgeBackSideNext;
                edgeLeft = edgeLeftSideNext;
                edgeRight = edgeRightSideNext;
            }
        
            for (int i =0;i< ncCode.Count; i++)
            {
                if (ncCode[i].Contains("}SIDE"))
                {
                    editSide1 = false;
                    editSide3 = false;
                    editSide4 = false;
                    editSide5 = false;
                    editSide6 = false;
                }
                if (ncCode[i].Contains("SIDE#1{"))
                {
                    editSide1 = true;
                }
                else if (ncCode[i].Contains("SIDE#3{"))
                {
                    editSide3 = true;
                }
                else if (ncCode[i].Contains("SIDE#4{"))
                {
                    editSide4 = true;
                }
                else if (ncCode[i].Contains("SIDE#5{"))
                {
                    editSide5 = true;
                }
                else if (ncCode[i].Contains("SIDE#6{"))
                {
                    editSide6 = true;
                }
                if (ncCode[i].Contains(contourFunctionName)|| ncCode[i].Contains(contourLineName) || ncCode[i].Contains(contourCurveName))
                {
                    
                    float tempX = 0;
                    float tempY = 0;
                    string[] temp = ncCode[i].Split(" ");
                    foreach (string currentLine in temp)
                    {
                        if (currentLine.Contains("#1=")) { tempX = getOnlyFloat(currentLine); continue; }
                        if (currentLine.Contains("#2=")) { tempY = getOnlyFloat(currentLine); break; }
                    }
                    if (ncCode[i].Contains(contourFunctionName))
                    {
                        startContourIndex = i;
                        if ((editSide1|| editSide3|| editSide5) && ((tempX <= 0 && edgeLeft) || (tempX >= componentLength && edgeRight))) startOnEdge = true;
                        else if  (editSide1 && ((tempY <= 0 && edgefront) || (tempY >= componentWidth && edgeBack))) startOnEdge = true;
                        else if ((editSide4 || editSide6) && ((tempX <= 0 && edgefront) || (tempX >= componentLength && edgeBack))) startOnEdge = true;
                        continue;
                    }
                    else if (!ncCode[i + 1].Contains(contourLineName) || !ncCode[i + 1].Contains(contourCurveName))
                    {
                        endContourIndex = i;
                        if ((editSide1 || editSide3 || editSide5) && ((tempX <= 0 && edgeLeft) || (tempX >= componentLength && edgeRight))) endOnEdge = true;
                        else if (editSide1 && ((tempY <= 0 && edgefront) || (tempY >= componentWidth && edgeBack))) endOnEdge = true;
                        else if ((editSide4 || editSide6) && ((tempX <= 0 && edgefront) || (tempX >= componentLength && edgeBack))) endOnEdge = true;
                        doChange = true;                                          
                        
                    }
                    else { continue; }                    
                }
                if (doChange && startContourIndex>0)
                {                   
                    if (startOnEdge && endOnEdge)
                    {
                       
                        if (endContourIndex-startContourIndex == 1)
                        {
                           
                            float tempX = 0;
                            float tempY = 0;

                            string[] tempLine = ncCode[startContourIndex + 1].Split(" ");

                            foreach (string currentLine in tempLine)
                            {
                                if (currentLine.Contains("#1=")){ tempX =  getOnlyFloat(currentLine); ncCode[startContourIndex + 1] = ncCode[startContourIndex + 1].Replace(currentLine, "#1=" + tempX / 2); continue; }
                                if (currentLine.Contains("#2=")) {tempY =  getOnlyFloat(currentLine); ncCode[startContourIndex + 1] = ncCode[startContourIndex + 1].Replace(currentLine, "#2=" + tempY / 2); break; }
                            }
                            string tempLine2 = ncCode[startContourIndex + 1];
                            if (tempLine2.Contains("#34=0")) tempLine2 = tempLine2.Replace("#34=0", "#34=1");
                            else if (tempLine2.Contains("#34=1")) tempLine2 = tempLine2.Replace("#34=1", "#34=0");
                            ncCode.Insert(startContourIndex + 2, tempLine2);
                            if (ncCode[startContourIndex].Contains("#8185=1")) ncCode[startContourIndex] = ncCode[startContourIndex].Replace("#8185=1", "#8185=0");
                            string tempFunctionNewLine = ncCode[startContourIndex];
                            string[] tempFunction = ncCode[startContourIndex].Split(" ");
                            foreach (string currentLine in tempFunction)
                            {
                                if (currentLine.Contains("#1=")) { tempFunctionNewLine = tempFunctionNewLine.Replace(currentLine, "#1=" + tempX); continue; }
                                if (currentLine.Contains("#2=")) { tempFunctionNewLine = tempFunctionNewLine.Replace(currentLine, "#2=" + tempY / 2); break; }
                            }
                            if (tempFunctionNewLine.Contains("#40=1")) tempFunctionNewLine = tempFunctionNewLine.Replace("#40=1", "#40=2");
                            else if (tempFunctionNewLine.Contains("#40=2")) tempFunctionNewLine = tempFunctionNewLine.Replace("#40=2", "#40=1");
                            ncCode.Insert(startContourIndex + 2, tempFunctionNewLine);
                            i ++;
                           
                        }
                        else if (endContourIndex - startContourIndex > 1)
                        {
                            testfehler = true;
                            float tempX = 0;
                            float tempY = 0;

                            string[] tempLine = ncCode[endContourIndex - 1].Split(" ");

                            foreach (string currentLine in tempLine)
                            {
                                if (currentLine.Contains("#1=")) { tempX = getOnlyFloat(currentLine); continue; }
                                if (currentLine.Contains("#2=")) { tempY = getOnlyFloat(currentLine); break; }
                            }
                            string[] tempLine2 = ncCode[endContourIndex].Split(" ");
                            foreach (string currentLine in tempLine2)
                            {
                               
                                if (currentLine.Contains("#1=")) { ncCode[endContourIndex] = ncCode[endContourIndex].Replace(currentLine, "#1=" + tempX); tempX = getOnlyFloat(currentLine); continue; }
                                if (currentLine.Contains("#2=")) { ncCode[endContourIndex] = ncCode[endContourIndex].Replace(currentLine, "#2=" + tempY); tempY = getOnlyFloat(currentLine); break; }
                            }
                            if (ncCode[endContourIndex].Contains("#34=0")) ncCode[endContourIndex] = ncCode[endContourIndex].Replace("#34=0", "#34=1");
                            else if (ncCode[endContourIndex].Contains("#34=1")) ncCode[endContourIndex] = ncCode[endContourIndex].Replace("#34=1", "#34=0");

                            if (ncCode[startContourIndex].Contains("#8185=1")) ncCode[startContourIndex] = ncCode[startContourIndex].Replace("#8185=1", "#8185=0");
                            string tempFunctionNewLine = ncCode[startContourIndex];
                            string[] tempFunction = ncCode[startContourIndex].Split(" ");
                            foreach (string currentLine in tempFunction)
                            {
                                if (currentLine.Contains("#1=")) { tempFunctionNewLine = tempFunctionNewLine.Replace(currentLine, "#1=" + tempX); continue; }
                                if (currentLine.Contains("#2=")) { tempFunctionNewLine = tempFunctionNewLine.Replace(currentLine, "#2=" + tempY); break; }
                            }
                            if (tempFunctionNewLine.Contains("#40=1")) tempFunctionNewLine = tempFunctionNewLine.Replace("#40=1", "#40=2");
                            else if (tempFunctionNewLine.Contains("#40=2")) tempFunctionNewLine = tempFunctionNewLine.Replace("#40=2", "#40=1");
                            ncCode.Insert(endContourIndex, tempFunctionNewLine);
                            i++;                            
                        }                       
                    }
                    else if (startOnEdge && !endOnEdge)
                    {
                       
                    }
                    else if (endOnEdge && !startOnEdge)
                    {
                        
                        float tempX = 0;
                        float tempY = 0;
                        string temp8180 = "";
                        string temp8181 = "";
                        string temp8182 = "";
                        string temp8184 = "";
                        bool changeEntryEdit = false;
                        string[] tempLine = ncCode[endContourIndex].Split(" ");

                        foreach (string currentLine in tempLine)
                        {
                            if (currentLine.Contains("#1=")) { tempX = getOnlyFloat(currentLine); continue; }
                            if (currentLine.Contains("#2=")) { tempY = getOnlyFloat(currentLine); break; }
                        }
                        if (!ncCode[startContourIndex].Contains("#8185=1") && !ncCode[startContourIndex].Contains("#8180=1")) changeEntryEdit = true; 

                        string[] tempFunction = ncCode[startContourIndex].Split(" ");
                        foreach (string currentLine in tempFunction)
                        {

                            if (currentLine.Contains("#1=")) { ncCode[startContourIndex] = ncCode[startContourIndex].Replace(currentLine, "#1=" + tempX); tempX = getOnlyFloat(currentLine); continue; }
                            if (currentLine.Contains("#2=")) { ncCode[startContourIndex] = ncCode[startContourIndex].Replace(currentLine, "#2=" + tempY); tempY = getOnlyFloat(currentLine); if (!changeEntryEdit) break; }
                            if (currentLine.Contains("#8180=")) temp8180 = currentLine; continue;
                            if (currentLine.Contains("#8181=")) temp8181 = currentLine; continue;
                            if (currentLine.Contains("#8182=")) temp8182 = currentLine; continue;
                            if (currentLine.Contains("#8184=")) temp8184 = currentLine; continue;
                            if (currentLine.Contains("#8185=")) { ncCode[startContourIndex] = ncCode[startContourIndex].Replace(temp8180, "#8180=" + currentLine.Replace("#8185=", "")).Replace(currentLine, "#8185=" + temp8180.Replace("#8180=", "")); continue; }
                            if (currentLine.Contains("#8186=")) { ncCode[startContourIndex] = ncCode[startContourIndex].Replace(temp8181, "#8181=" + currentLine.Replace("#8186=", "")).Replace(currentLine, "#8186=" + temp8181.Replace("#8181=", "")); continue; }
                            if (currentLine.Contains("#8187=")) { ncCode[startContourIndex] = ncCode[startContourIndex].Replace(temp8182, "#8182=" + currentLine.Replace("#8187=", "")).Replace(currentLine, "#8187=" + temp8182.Replace("#8182=", "")); continue; }
                            if (currentLine.Contains("#8189=")) { ncCode[startContourIndex] = ncCode[startContourIndex].Replace(temp8184, "#8184=" + currentLine.Replace("#8189=", "")).Replace(currentLine, "#8189=" + temp8184.Replace("#8184=", "")); break; }

                        }
                        if (ncCode[startContourIndex].Contains("#40=1")) ncCode[startContourIndex] = ncCode[startContourIndex].Replace("#40=1", "#40=2");
                        else if (ncCode[startContourIndex].Contains("#40=2")) ncCode[startContourIndex] = ncCode[startContourIndex].Replace("#40=2", "#40=1");
                        for (int j = 0; j < (endContourIndex - startContourIndex); j++)
                        {

                            string[] tempLine2 = ncCode[startContourIndex + 1].Split(" ");

                            foreach (string currentLine in tempLine2)
                            {
                                if (currentLine.Contains("#1=")) { ncCode[startContourIndex + 1] = ncCode[startContourIndex + 1].Replace(currentLine, "#1=" + tempX); tempX = getOnlyFloat(currentLine); continue; }
                                if (currentLine.Contains("#2=")) { ncCode[startContourIndex + 1] = ncCode[startContourIndex + 1].Replace(currentLine, "#2=" + tempY); tempY = getOnlyFloat(currentLine); break; }
                            }
                            if (ncCode[startContourIndex + 1].Contains("#34=0")) ncCode[startContourIndex + 1] = ncCode[startContourIndex + 1].Replace("#34=0", "#34=1");
                            else if (ncCode[startContourIndex + 1].Contains("#34=1")) ncCode[startContourIndex + 1] = ncCode[startContourIndex + 1].Replace("#34=1", "#34=0");
                            if (j < (endContourIndex - startContourIndex - 1))
                            {
                                ncCode.Insert(endContourIndex + 1 - j, ncCode[startContourIndex + 1]);
                                ncCode.RemoveAt(startContourIndex + 1);
                            }
                        }
                        
                       
                    }
                    
                        startOnEdge = false;
                        endOnEdge = false;
                        doChange = false;
                        startContourIndex = 0;
                        endContourIndex = 0;
                        
                    
                }
            }
            return ncCode;
        }
        private string createNewFileName( string fileName, string newPath, string componentNr, string side)
        {
            if (fileName != "")
            {
                string firstLettersToTakeOver = "";                               

                FileInfo fi = new FileInfo(oldNCPath+fileName);
                string fileExtension = fi.Extension;               
                string fileNameTemp = fileName.Replace(side + fileExtension, "");               
                fileNameTemp = fileNameTemp.Replace("F" + fileExtension, "");
                char[] temp = fileNameTemp.ToCharArray();

                if (temp.Length >= firstNumberOfLettersToTakeOver+lastNumberOfLettersToTakeOver)
                {
                    for (int i = 0; i < firstNumberOfLettersToTakeOver; i++)
                    {
                        firstLettersToTakeOver += temp[i];
                    }
                    for(int i = temp.Length-lastNumberOfLettersToTakeOver-1; i < temp.Length; i++)
                    {
                        firstLettersToTakeOver += temp[i];
                    }
                }

                else firstLettersToTakeOver = fileNameTemp;
                string newFileName = componentNr + firstLettersToTakeOver + side + fileExtension;
                
                return newPath + @"\" + newFileName;
            }
            else return "";
        }
        private float getOnlyFloat(string stringWithNumber)
        {            
            var stringOnlyNumber = string.Join(string.Empty, Regex.Matches(stringWithNumber, @"=+.*").OfType<Match>().Select(m => m.Value));
            if (stringOnlyNumber.Length == 3 && stringOnlyNumber.Contains("=-0"))
            {
                stringOnlyNumber = stringOnlyNumber.Replace("-", "");
            }
            try
            {
                float test = Convert.ToSingle(stringOnlyNumber.Replace("=", ""));
            }
            catch
            {
                MessageBox.Show("" + testfehlerindex + testfehler+ncMain); 
            }
            return Convert.ToSingle(stringOnlyNumber.Replace("=",""));
        }
        public string getNCMainPath()
        {
            return ncMain;
        }
        public string getNCMain2Path()
        {
            return ncMain2;
        }
        public string getNCNextPath()
        {
            return ncNext;
        }
        public string getNCNext2Path()
        {
            return ncNext2;
        }
        public List<string> getControllList()
        {
            return controllList;
        }
        public float getHingedSide()
        {
            return componentLength;
        }
    }
}
