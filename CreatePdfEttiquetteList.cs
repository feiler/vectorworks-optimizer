﻿using iText.Barcodes;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Action;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Kernel.Font;
using System;
using System.Collections.Generic;
using System.Text;
using iText.IO.Font;
using iText.IO.Colors;
using iText.Kernel.Pdf.Canvas;

namespace Vectorworks_Optimizer
{
    class CreatePdfEttiquetteList
    {
        PdfDocument pdfDoc;
        Document doc;
        PdfFont fontBold = PdfFontFactory.CreateFont(FontConstants.HELVETICA_BOLD);
        Table table = new Table(UnitValue.CreatePercentArray(new float[] { 1f, 1f, 1f })).UseAllAvailableWidth();
        float mmxUserUnit = 2.93f;
        
        private string imageTriangle = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)+@"\triangle.jpg";
        private string imageHatchingFiber = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\hatching_fiber_course.jpg";

        private string groundPath = OptionGeneralSetting.Default.rootFolderCNC;

       
        public void pOpen(string path, string pdfName)
        {

            PdfWriter writer = new PdfWriter(path + @"\" + pdfName + ".pdf");
            pdfDoc = new PdfDocument(writer);
            pdfDoc.SetDefaultPageSize(PageSize.A4);
            PdfPage pdfPage = pdfDoc.AddNewPage();

            doc = new Document(pdfDoc).SetFontSize(7);
            doc.SetMargins(15f*mmxUserUnit, 6f * mmxUserUnit, 14f * mmxUserUnit, 6f * mmxUserUnit);
            

        }
        public void createEttiquette(
            string typeName,
            string numberName,
            string ComponentNr,
            string projectNr,
            string projectPos,
            string furniturName,
            string carrierMaterial,
            string componentDefinition,
            int count,
            float finalLength, float finalWidth, float thick,
            float cuttingLength, float cuttingWidth,
            bool rotatable,
            string componentName,
            string edgeFront, string edgeBack,
            string edgeLeft, string edgeRight,
            
            string polygonalEdge, string existEdge,
            string surfaceOne, string surfaceTwo,
            string coating,
            string cnMain, string cnNext, string cnMain2, string cnNext2,
            string customerLastName,
            string getBasicMaterialNumber,
            bool hingedSideLength)
        {
            
           
            Image imgTriangle = new Image(ImageDataFactory.Create(imageTriangle));
            Image imgHatchingFiber = new Image(ImageDataFactory.Create(imageHatchingFiber));


            Table tableGround = new Table(UnitValue.CreatePercentArray(new float[] { 1f,12f,1f })).UseAllAvailableWidth().SetHeight(108f).SetMaxHeight(108f);
            tableGround.SetMargin(-2.25f);
            if (!hingedSideLength) tableGround.AddCell(new Cell().SetPaddingRight(0).SetVerticalAlignment(VerticalAlignment.MIDDLE).SetFont(fontBold).SetFontColor(ColorConstants.RED).Add(new Paragraph(cuttingWidth + " || " + edgeLeft).SetRotationAngle(-Math.PI / 2)).SetBorder(Border.NO_BORDER));
            else tableGround.AddCell(new Cell().SetPaddingRight(0).SetVerticalAlignment(VerticalAlignment.MIDDLE).Add(new Paragraph(cuttingWidth + " || " + edgeLeft).SetRotationAngle(-Math.PI / 2)).SetBorder(Border.NO_BORDER));

            Table tableMiddlePart = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth().SetMargin(-2.25f);
           
            tableMiddlePart.AddCell(new Cell().SetPadding(0).SetHeight(10f).SetTextAlignment(TextAlignment.CENTER).Add(new Paragraph(edgeBack + "   ").SetRotationAngle(Math.PI)).SetBorder(Border.NO_BORDER));

            Table tableCenter = new Table(UnitValue.CreatePercentArray(new float[] { 1f, 4f, 1f })).UseAllAvailableWidth().SetHeight(87f).SetMargin(-2.25f);

            float heightQRQuadre = 25f;
            Table tableCenterLeft = new Table(1).UseAllAvailableWidth().SetHeight(87f).SetMargin(-2.25f);
            if (cnMain == "") tableCenterLeft.AddCell(new Cell().SetHeight(heightQRQuadre).Add(new Paragraph(" ")).SetBorder(Border.NO_BORDER));
            else tableCenterLeft.AddCell(new Cell().Add(qrCodeGenerate(cnMain + OptionGeneralSetting.Default.ncCodeEnd)).SetPadding(-4).SetBorder(Border.NO_BORDER));

            if (rotatable) tableCenterLeft.AddCell(new Cell().SetHeight(heightQRQuadre).Add(new Paragraph(" ")).SetBorder(Border.NO_BORDER));
            else tableCenterLeft.AddCell(new Cell().SetHeight(heightQRQuadre).Add(imgHatchingFiber.SetAutoScale(true)).SetBorder(Border.NO_BORDER));

            if (cnMain2 == "") tableCenterLeft.AddCell(new Cell().SetHeight(heightQRQuadre).Add(new Paragraph(" ")).SetBorder(Border.NO_BORDER));
            else tableCenterLeft.AddCell(new Cell().Add(qrCodeGenerate(cnMain2 + OptionGeneralSetting.Default.ncCodeEnd)).SetPadding(-4).SetBorder(Border.NO_BORDER));
            tableCenter.AddCell(new Cell().Add(tableCenterLeft).SetBorder(Border.NO_BORDER));

            Table tableCenterMiddle = new Table(UnitValue.CreatePercentArray(1)).UseAllAvailableWidth();
            tableCenterMiddle.AddCell(new Cell().Add(new Paragraph("Auftrag: " + projectNr + "  " + customerLastName)));
            tableCenterMiddle.AddCell(new Cell().Add(new Paragraph(numberName+": " + ComponentNr + "   " + getBasicMaterialNumber)).SetBorder(Border.NO_BORDER));
            if (furniturName == "")
            {
                tableCenterMiddle.AddCell(new Cell().Add(new Paragraph("Zu: " + componentDefinition)).SetMaxHeight(12f));
            }
            else
                tableCenterMiddle.AddCell(new Cell().Add(new Paragraph("Zu: " + furniturName)).SetMaxHeight(12f));
            tableCenterMiddle.AddCell(new Cell().Add(new Paragraph(typeName+": " + componentName)).SetBorder(Border.NO_BORDER).SetMaxHeight(25f));
            tableCenter.AddCell(new Cell().Add(tableCenterMiddle).SetBorder(Border.NO_BORDER));


            Table tableCenterRight = new Table(1).UseAllAvailableWidth().SetHeight(87f).SetMargin(-2.25f);
            if (cnNext == "") tableCenterRight.AddCell(new Cell().SetHeight(heightQRQuadre).Add(new Paragraph(" ")).SetBorder(Border.NO_BORDER));
            else tableCenterRight.AddCell(new Cell().Add(qrCodeGenerate(cnNext + OptionGeneralSetting.Default.ncCodeEnd)).SetPadding(-4).SetBorder(Border.NO_BORDER));
            tableCenterRight.AddCell(new Cell().SetHeight(heightQRQuadre).Add(imgTriangle.SetAutoScale(true)).SetBorder(Border.NO_BORDER));
            if (cnNext2 == "") tableCenterRight.AddCell(new Cell().SetHeight(heightQRQuadre).Add(new Paragraph(" ")).SetBorder(Border.NO_BORDER));
            else tableCenterRight.AddCell(new Cell().Add(qrCodeGenerate(cnNext2 + OptionGeneralSetting.Default.ncCodeEnd)).SetPadding(-4).SetBorder(Border.NO_BORDER));
            tableCenter.AddCell(new Cell().Add(tableCenterRight).SetBorder(Border.NO_BORDER));


            tableMiddlePart.AddCell(new Cell().SetHeight(82f).Add(tableCenter).SetBorder(Border.NO_BORDER));


            if(hingedSideLength) tableMiddlePart.AddCell(new Cell().SetPadding(0).SetHeight(10f).SetTextAlignment(TextAlignment.CENTER).SetFont(fontBold).SetFontColor(ColorConstants.RED).Add(new Paragraph(cuttingLength + " || " + edgeFront)).SetBorder(Border.NO_BORDER));
            else tableMiddlePart.AddCell(new Cell().SetPadding(0).SetHeight(10f).SetTextAlignment(TextAlignment.CENTER).Add(new Paragraph(cuttingLength + " || " + edgeFront)).SetBorder(Border.NO_BORDER));
           
            tableGround.AddCell(new Cell().Add(tableMiddlePart).SetBorder(Border.NO_BORDER));
            tableGround.AddCell(new Cell().SetPadding(0).SetVerticalAlignment(VerticalAlignment.MIDDLE).Add(new Paragraph(edgeRight).SetRotationAngle(Math.PI / 2)).SetBorder(Border.NO_BORDER));
            table.AddCell(new Cell().Add(tableGround).SetBorder(Border.NO_BORDER));

        }
        public void freePlaceEttiquette()
        {
            table.AddCell(new Cell().SetHeight(108f).SetBorder(Border.NO_BORDER));
        }  
        public void AddTable()
        {
            doc.Add(table);
        }

        public void pClose()
        {
            doc.Close();
        }
        
        private Image qrCodeGenerate(string ncCode)
        {
            BarcodeQRCode barcodeQRCode = new BarcodeQRCode();
            barcodeQRCode.SetCode(ncCode);
            Image qrCodeImeage = new Image(barcodeQRCode.CreateFormXObject(pdfDoc));
            qrCodeImeage.ScaleAbsolute(34, 34);
            //qrCodeImeage.ScaleToFit(34, 34);
            return qrCodeImeage;
        }
    }
}
