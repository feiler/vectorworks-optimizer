﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CsvHelper;
using System.Globalization;
using CsvHelper.Configuration.Attributes;
using Microsoft.Win32;
using System.IO;


namespace Vectorworks_Optimizer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<string> materialList = new List<string>();
     
        private List<ConstructorMaterialList> constructorMaterialLists = new List<ConstructorMaterialList>();
        private List<ConstructorFittingList> constructorFittingLists = new List<ConstructorFittingList>();
        private List<ConstructorSurfaceList> constructorSurfaceLists = new List<ConstructorSurfaceList>();
        private List<ConstructorMaterialChanger> constructorMaterialChangers = new List<ConstructorMaterialChanger>();
        
        private CsvReader csvMaterialList;
        
        private string targetMaterialListName = "ZuschnittK.txt";
        private string mainRootfolder = "";
        private string groundfolder = "";
        private string cuttingListRootFolder = "";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void bt_updateSelectDictionary_Click(object sender, RoutedEventArgs e)
        {
            CheckNewCNCFolder checkNewCNCFolder = new CheckNewCNCFolder();
            tb_selectDictionary.Text = checkNewCNCFolder.getNewestCuttingList(targetMaterialListName);

        }

        private void bt_openCuttingFile_Click(object sender, RoutedEventArgs e)
        {
            CheckNewCNCFolder checkNewCNCFolder = new CheckNewCNCFolder();
            OpenFileDialog openFileDlg = new OpenFileDialog();
            //openFileDlg.DefaultExt = ".txt";
            openFileDlg.Filter = "Text files (*ZuschnittK.txt)|*ZuschnittK.txt";
            openFileDlg.InitialDirectory = OptionGeneralSetting.Default.rootFolderCNC;
            // Launch OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = openFileDlg.ShowDialog();
            // Get the selected file name and display in a TextBox.
            // Load content of file in a TextBlock
            if (result == true)
            {
                string temp = openFileDlg.FileName;
               
                tb_selectDictionary.Text = new FileInfo(temp).Directory.FullName;
              
            }
        }
        private void bt_readSelectDictionary_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists(tb_selectDictionary.Text + @"\" + targetMaterialListName))
            {
                CheckNewCNCFolder checkNewCNCFolder = new CheckNewCNCFolder();
                mainRootfolder = checkNewCNCFolder.getMainRootFolder(tb_selectDictionary.Text);
                groundfolder = checkNewCNCFolder.getGroundRootFolder(tb_selectDictionary.Text);
                readTxtList readTxtList = new readTxtList();

                readTxtList.openRead(tb_selectDictionary.Text + @"\" + targetMaterialListName);
                constructorMaterialLists = readTxtList.getMaterialList();
                constructorFittingLists = readTxtList.getFittingList();
                constructorSurfaceLists = readTxtList.getSurfaceList();
                constructorMaterialChangers = readTxtList.getMaterialChangeList();
                MessageBox.Show("gelesen");
            }
            else { MessageBox.Show("Keine Zuschnittsliste gefunden"); }
        }

        private void bt_changeMaterial_Click(object sender, RoutedEventArgs e)
        {
            if (constructorMaterialChangers.Count != 0)
            {
                MaterialChanger materialChanger = new MaterialChanger();
                materialChanger.openMaterialChanger(constructorMaterialChangers, tb_selectDictionary.Text);
                materialChanger.ShowDialog();
                constructorMaterialChangers = materialChanger.getNewMaterialChanger();
                
            }
            else MessageBox.Show("keine Liste geladen");

        }

        private void bt_writelists_Click(object sender, RoutedEventArgs e)
        {
            if (constructorMaterialLists.Count > 0) { 
            NamePdfList namePdfList = new NamePdfList();
            EttiquetteJumpOverSpace ettiquetteJumpOverSpace = new EttiquetteJumpOverSpace();
            namePdfList.ShowDialog();
            string namePdfAddition = namePdfList.getPdfAdditionName();

            if (namePdfAddition != "") namePdfAddition = namePdfAddition+"_";

            ettiquetteJumpOverSpace.ShowDialog();

            CreatePdfList createPdfList = new CreatePdfList();
            createPdfList.loadList(constructorMaterialLists, constructorSurfaceLists);
            createPdfList.sortListAndChangeMaterial(constructorMaterialChangers);
            createPdfList.distributeMaterial(tb_selectDictionary.Text, namePdfAddition+ "Werkstoffliste", mainRootfolder, groundfolder);
            if (constructorSurfaceLists.Count != 0) createPdfList.distributeSurface(tb_selectDictionary.Text, namePdfAddition+ "Belag", mainRootfolder, groundfolder);
            if (constructorFittingLists.Count != 0) createPdfList.distributeFitting(constructorFittingLists, tb_selectDictionary.Text,namePdfAddition+ "Beschlag");
                        
            createPdfList.distributeEttiquette(tb_selectDictionary.Text, namePdfAddition+"Ettiketten", ettiquetteJumpOverSpace.getPdfNumber());

            createPdfList.distributeOptimizList(tb_selectDictionary.Text,namePdfAddition, constructorMaterialChangers);
            
            MessageBox.Show("geschrieben");
            }
            else MessageBox.Show("keine Liste geladen");
        }

        
        private void bt_GeneralOption_Click(object sender, RoutedEventArgs e)
        {
            OptionGeneral _optionGeneral = new OptionGeneral();
            _optionGeneral.Show();
        }

        private void bt_OptionCNCOptimization_Click(object sender, RoutedEventArgs e)
        {
            OptionOptimizerCNC optionOptimizerCNCs = new OptionOptimizerCNC();
            optionOptimizerCNCs.Show();
        }


        //old
      
        private void testCreater()
        {
            var csvlist = csvMaterialList.GetRecords<ReadMaterialList>();
            CreatePdfMaterialList createPdfMaterialList = new CreatePdfMaterialList();
            createPdfMaterialList.pOpen(tb_selectDictionary.Text, "Werkstoffliste");

            bool fillHeader = true;
            
            
                
        
            
        }
        private void createPdfFitting()
        {
            var csvlist = csvMaterialList.GetRecords<ReadMaterialList>();
            //if(csvlist.Contains(csvlist))
            CreatePdfFittingList createPdfFittingList = new CreatePdfFittingList();
            createPdfFittingList.pOpen(tb_selectDictionary.Text, "Beschlagsliste");
            createPdfFittingList.pClose();
            MessageBox.Show("erstellt");
        }

        private void button_InstallCuttingList_Click(object sender, RoutedEventArgs e)
        {
            InstallCuttingList installCuttingList = new InstallCuttingList();
            installCuttingList.ShowDialog();
        }
    }
}
