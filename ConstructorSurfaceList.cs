﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vectorworks_Optimizer
{
    class ConstructorSurfaceList
    {
        public int ComponentNumber { get; set; }
        public string ProjectID { get; set; }
        public string PojectPos { get; set; }
        public string PosDescription { get; set; }
        public string FurnitureName { get; set; }
        public string ComponentDefinition { get; set; }
        public string ComponentName { get; set; }
        public string CarrierMaterial { get; set; }
        public int Count { get; set; }
        public string Thick { get; set; }
        public float CuttingLength { get; set; }
        public float CuttingWidth { get; set; }
        public string Rotatable { get; set; }
        public string Side { get; set; }
        public string EdgeFront { get; set; }
        public string EdgeBack { get; set; }
        public string EdgeLeft { get; set; }
        public string EdgeRight { get; set; }
        public string EdgeCode { get; set; }
        public string NC1 { get; set; }
        public string NC2 { get; set; }
        public string CustomerLastName { get; set; }
        public string ProjectName { get; set; }
        public string FileName { get; set; }
        public string EditType { get; set; }
        public int getBasicMaterialNumber { get; set; }
        public bool HingedSideLength { get; set; }
    }
}
