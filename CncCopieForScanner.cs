﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;

namespace Vectorworks_Optimizer
{
    class CncCopieForScanner
    {
        private int numberOfLettersToTakeOver = 5;
        public string editNameCopyGetNewPath(string oldPath, string positionNr,string furniturName,string fileName, string newPath , int componentNr, string side)
        {
            if (fileName != "")
            {
                string stringBuilderForOldFile;
                string firstLettersToTakeOver = "";
                string positionNrBuild = "";
                string furniturNameBuild = "";

                if (positionNr != "") positionNrBuild = @"\" + positionNr;
                if (furniturName != "") furniturNameBuild = @"\" + furniturName;

                stringBuilderForOldFile = oldPath + positionNrBuild + furniturNameBuild + @"\" + fileName;

                FileInfo fi = new FileInfo(stringBuilderForOldFile);
                string fileExtension = fi.Extension;
                string fileNameTemp = fileName.Replace(side + fileExtension, "");
                char[] temp = fileNameTemp.ToCharArray();

                if (temp.Length >= numberOfLettersToTakeOver)
                {
                    for (int i = 0; i < numberOfLettersToTakeOver; i++)
                    {
                        firstLettersToTakeOver += temp[i];
                    }
                }
                else firstLettersToTakeOver = fileNameTemp;
                string newFileName = componentNr + firstLettersToTakeOver + side + fileExtension;
                File.Copy(stringBuilderForOldFile, newPath + @"\" + newFileName, true);
                return newPath + @"\" + newFileName;
            }
            else return "";
        }
    }
}
