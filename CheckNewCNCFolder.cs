﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Windows;

namespace Vectorworks_Optimizer
{
    class CheckNewCNCFolder
    {
        string searchFildeName = "ZuschnittK.txt";
        
        public string getNewestCuttingList(string searchFileName)
        {
            string newestCuttingListPath = "";
            
            DirectoryInfo di = new DirectoryInfo(OptionGeneralSetting.Default.rootFolderCNC);
            DirectoryInfo[] directories =
                di.GetDirectories(searchFileName, SearchOption.AllDirectories);

            FileInfo[] files =
                di.GetFiles(searchFileName, SearchOption.AllDirectories);
                  
                       
            foreach (FileInfo file in files)
            {
                if (newestCuttingListPath == "")
                {
                    newestCuttingListPath = file.FullName;
                   
                }
                else
                {
                    int compareResult = DateTime.Compare(File.GetLastWriteTime(newestCuttingListPath), File.GetLastWriteTime(file.FullName));
                    if (compareResult < 0) newestCuttingListPath = file.FullName;
                }
            }
            //new FileInfo(newestCuttingListPath).Directory.FullName
            return new FileInfo(newestCuttingListPath).Directory.FullName;
        }
        public string getMainFolder(string holeFolder)
        {
            holeFolder = holeFolder.Replace(OptionGeneralSetting.Default.rootFolderCNC, "");
            string[] temp = holeFolder.Split(@"\");

            return temp[1];
        }
        public string getGroundRootFolder(string holeFolder)
        {
            holeFolder = holeFolder.Replace(OptionGeneralSetting.Default.rootFolderCNC, "");
            string[] temp = holeFolder.Split(@"\");

            return OptionGeneralSetting.Default.rootFolderCNC+ @"\"+temp[1];
        }
        public string getMainRootFolder(string holeFolder)
        {
            holeFolder = holeFolder.Replace(OptionGeneralSetting.Default.rootFolderCNC, "");
            string[] temp = holeFolder.Split(@"\");

            return OptionGeneralSetting.Default.rootFolderCNC + @"\" + temp[1] + @"\" + temp[2];
        }
    }
}
